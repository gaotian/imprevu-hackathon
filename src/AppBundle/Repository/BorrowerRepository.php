<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BorrowerRepository extends EntityRepository
{

    //Nombre total de borrowers pour une bibliothèque
    public function totalBorrowerFor1library($library){
        return $this->getEntityManager()->createQuery("SELECT COUNT(b) as nb FROM AppBundle:Borrower b JOIN b.library l WHERE l.idlibrary = :library")
            ->setParameter('library', $library)
            ->getSingleResult();
    }

    public function totalEtudForAYear($library, $year){
        return $this->getEntityManager()->createQuery("SELECT 'etudiants' as label,  COUNT(DISTINCT(i.idborrower) as value FROM AppBundle:Borrower b JOIN b.library l JOIN b.issues i WHERE l.idlibrary = :library AND i.borrower_type = :code1 OR i.borrower_type = :code2 AND i.issuedate LIKE :year")
            ->setParameter('library', $library)
            ->setParameter('year', '%'.$year.'%')
            ->setParameter('code1', 'D')
            ->setParameter('code2', 'LM')
            ->getSingleResult();
    }

    public function totalChercheurForAYear($library, $year){
        return $this->getEntityManager()->createQuery("SELECT 'cherheurs' as label , COUNT(DISTINCT(i.idborrower) as value FROM AppBundle:Borrower b JOIN b.library l JOIN b.issues i WHERE l.idlibrary = :library AND i.borrower_type = :code1 AND i.issuedate LIKE :year")
            ->setParameter('library', $library)
            ->setParameter('year', '%'.$year.'%')
            ->setParameter('code1', 'EN-CH')
            ->getSingleResult();
    }

    public function totalBorrowerFor1libraryForAYear($library, $year){
        return $this->getEntityManager()->createQuery("SELECT COUNT(DISTINCT(i.idborrower) as nb FROM AppBundle:Borrower b JOIN b.library l JOIN b.issues i WHERE l.idlibrary = :library AND i.borrower_type != :code1 AND i.borrower_type != :code2 AND i.issuedate LIKE :year")
            ->setParameter('library', $library)
            ->setParameter('year', '%'.$year.'%')
            ->setParameter('code1', 'AGR')
            ->setParameter('code2', 'BIB')
            ->getSingleResult();
    }

}



