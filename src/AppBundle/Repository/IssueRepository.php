<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class IssueRepository extends EntityRepository
{

    //Nombre de prêts pour une bibliothèque
    public function totalIssueFor1library($library){
        return $this->getEntityManager()->createQuery("SELECT COUNT(i) as nb FROM AppBundle:Issue i JOIN i.idlibrary l WHERE l.idlibrary = :library")
            ->setParameter('library', $library)
            ->getSingleResult();
    }


    public function totalIssueFor1libraryForAYear($library, $year){
        return $this->getEntityManager()->createQuery("SELECT COUNT(i) as nb FROM AppBundle:Issue i JOIN i.idlibrary l WHERE l.idlibrary = :library AND i.issuedate LIKE :year")
            ->setParameter('library', $library)
            ->setParameter('year', '%'.$year.'%')
            ->getSingleResult();
    }

    public function top100BooksFor1library($library){
        return $this->getEntityManager()->createQuery("SELECT b.title, COUNT(b.id) as nb, b.id as id FROM AppBundle:Issue i JOIN i.idlibrary l JOIN i.iditem t JOIN t.idbook b WHERE l.idlibrary = :library GROUP BY b.id ORDER BY nb DESC")
//        return $this->getEntityManager()->createQuery("SELECT b.title, b.id, COUNT(b.id) as nb FROM AppBundle:Issue i JOIN i.iditem t JOIN t.idbook b WHERE i.idlibrary = :library GROUP BY i.iditem ORDER BY nb DESC")
            ->setParameter('library', $library)
            ->setMaxResults(100)
            ->getResult();
    }

    public function top100BooksFor1libraryForAYear($library, $year){
        return $this->getEntityManager()->createQuery("SELECT b.title, COUNT(b.id) as nb, b.id as id FROM AppBundle:Issue i JOIN i.idlibrary l JOIN i.iditem t JOIN t.idbook b WHERE l.idlibrary = :library  AND i.issuedate LIKE :year  GROUP BY b.id ORDER BY nb DESC")
            ->setParameter('library', $library)
            ->setParameter('year', $year)
            ->setMaxResults(100)
            ->getResult();
    }

    public function nbIssueByDayForAYear($year, $library){

        $nbIssueByDayForAYear = $this->getEntityManager()->createQuery("SELECT UNIX_TIMESTAMP(i.issuedate) as label, COUNT(i) as value FROM AppBundle:Issue i WHERE i.idlibrary = :library AND i.borrower_type != :code1 AND i.borrower_type != :code2 AND i.issuedate LIKE :year GROUP BY i.issuedate ORDER BY label")
            ->setParameter('library', $library)
            ->setParameter('code1', 'AGR')
            ->setParameter('code2', 'BIB')
            ->setParameter('year', '%'.$year.'%')
            ->getResult();

        foreach ($nbIssueByDayForAYear as $key => $item) {
            $nbIssueByDayForAYear[$key]['value'] = (int)$nbIssueByDayForAYear[$key]['value'];
            $nbIssueByDayForAYear[$key]['label'] = (int)$nbIssueByDayForAYear[$key]['label'];

        }

        return $nbIssueByDayForAYear;
    }

    public function nbEmprunteursByEtapeForALibrary($library, $year){
        $response = $this->getEntityManager()->createQuery("SELECT i.etape as label, COUNT(i) as value FROM AppBundle:Issue i WHERE i.idlibrary = :library AND (i.borrower_type = :code1 OR i.borrower_type = :code2) AND i.issuedate LIKE :year AND i.etape != '' GROUP BY i.etape ORDER BY value DESC")
            ->setParameter('library', $library)
            ->setParameter('code1', 'LM')
            ->setParameter('code2', 'D')
            ->setParameter('year', '%'.$year.'%')
            ->getResult();

        return $response;
    }

    public function nbEmprunteursByNiveauForALibrary($library, $year){
        $response = $this->getEntityManager()->createQuery("SELECT i.niveau as label, COUNT(i) as value FROM AppBundle:Issue i WHERE i.idlibrary = :library AND (i.borrower_type = :code1 OR i.borrower_type = :code2) AND i.issuedate LIKE :year AND i.niveau != '' GROUP BY i.niveau ORDER BY value DESC")
            ->setParameter('library', $library)
            ->setParameter('code1', 'LM')
            ->setParameter('code2', 'D')
            ->setParameter('year', '%'.$year.'%')
            ->getResult();

        return $response;
    }

}



