<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class LibraryRepository extends EntityRepository
{
    //Nombre bibliothèques
    public function nbLibrary(){

        return $this->getEntityManager()->createQuery("SELECT COUNT(l) as nb FROM AppBundle:Library l")
            ->getSingleResult();
//            ->execute();
    }
}



