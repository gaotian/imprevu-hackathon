<?php

namespace AppBundle\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class BibliovizzController extends Controller
{
    /**
     * @Route("/bibliovizz/annee={year}&biblio={library}", name="bibliovizz")
     */
    public function indexAction($year, $library)
    {

            $em = $this->getDoctrine()->getManager();
            $totalBorrowerFor1libraryForAYear = $em->getRepository('AppBundle:Borrower')->totalBorrowerFor1libraryForAYear($library, $year);
            $totalBorrowerFor1libraryFor1YearBefore = $em->getRepository('AppBundle:Borrower')->totalBorrowerFor1libraryForAYear($library, ($year-1));
            $totalBorrowerFor1libraryFor2YearBefore = $em->getRepository('AppBundle:Borrower')->totalBorrowerFor1libraryForAYear($library, ($year-2));

            $nbEmprunteursByEtapeForALibrary = $em->getRepository('AppBundle:Issue')->nbEmprunteursByEtapeForALibrary($library, $year);
            $nbEmprunteursByNiveauForALibrary = $em->getRepository('AppBundle:Issue')->nbEmprunteursByNiveauForALibrary($library, $year);
            $top100BooksFor1libraryForAYear = $em->getRepository('AppBundle:Issue')->top100BooksFor1libraryForAYear($library, $year);
            $totalIssueFor1libraryForAYear = $em->getRepository('AppBundle:Issue')->totalIssueFor1libraryForAYear($library, $year);
            $totalIssueFor1libraryFor1YearBefore = $em->getRepository('AppBundle:Issue')->totalIssueFor1libraryForAYear($library, ($year-1));
            $totalIssueFor1libraryFor2YearBefore = $em->getRepository('AppBundle:Issue')->totalIssueFor1libraryForAYear($library, ($year-2));
            $myLibrary = $em->getRepository('AppBundle:Library')->findOneByIdlibrary($library);

            $totalEtudForAYear = $em->getRepository('AppBundle:Borrower')->totalEtudForAYear($library, $year);
            $totalChercheurForAYear = $em->getRepository('AppBundle:Borrower')->totalChercheurForAYear($library, $year);

        return $this->render('AppBundle:Bibliovizz:bibliovizz.html.twig', array(
            'year' => $year,
            'library' => $library,
            'totalBorrowerFor1library' => $totalBorrowerFor1libraryForAYear,
            'totalBorrowerFor1libraryFor1YearBefore'=> $totalBorrowerFor1libraryFor1YearBefore,
            'totalBorrowerFor1libraryFor2YearBefore' => $totalBorrowerFor1libraryFor2YearBefore,
            'nbEmprunteursByEtapeForALibrary' => $nbEmprunteursByEtapeForALibrary,
            'nbEmprunteursByNiveauForALibrary' => $nbEmprunteursByNiveauForALibrary,
            'top100BooksFor1libraryForAYear' => $top100BooksFor1libraryForAYear,
            'totalIssueFor1libraryForAYear' => $totalIssueFor1libraryForAYear,
            'totalIssueFor1libraryFor1YearBefore' => $totalIssueFor1libraryFor1YearBefore,
            'totalIssueFor1libraryFor2YearBefore' => $totalIssueFor1libraryFor2YearBefore,
            'totalChercheurForAYear' => $totalChercheurForAYear,
            'totalEtudForAYear' => $totalEtudForAYear,
            'myLibrary' => $myLibrary
        ));
    }

    /**
     * @Route("/bibliovizz/page{page}/annee={year}&biblio={library}", name="bibliovizz_page")
     */
    public function readAction($page, $year, $library){

        $em = $this->getDoctrine()->getManager();


//        $top100BooksFor1libraryForAYear = $em->getRepository('AppBundle:Issue')->top100BooksFor1libraryForAYear($library, $year);
       $myLibrary = $em->getRepository('AppBundle:Library')->findOneByIdlibrary($library);


//        $totalEtudForAYear = $em->getRepository('AppBundle:Borrower')->totalEtudForAYear($library, $year);
//        $totalChercheurForAYear = $em->getRepository('AppBundle:Borrower')->totalChercheurForAYear($library, $year);


        switch ($page) {
            case 1:
                return $this->render('AppBundle:Bibliovizz:page1.html.twig', array(
                    'year' => $year,
                    'library' => $library,
                    'myLibrary' => $myLibrary
                ));
                break;
            case 2:
                $totalBorrowerFor1libraryForAYear = $em->getRepository('AppBundle:Borrower')->totalBorrowerFor1libraryForAYear($library, $year);
                $totalBorrowerFor1libraryFor1YearBefore = $em->getRepository('AppBundle:Borrower')->totalBorrowerFor1libraryForAYear($library, ($year-1));
                $totalBorrowerFor1libraryFor2YearBefore = $em->getRepository('AppBundle:Borrower')->totalBorrowerFor1libraryForAYear($library, ($year-2));
                $totalIssueFor1libraryForAYear = $em->getRepository('AppBundle:Issue')->totalIssueFor1libraryForAYear($library, $year);
                $totalIssueFor1libraryFor1YearBefore = $em->getRepository('AppBundle:Issue')->totalIssueFor1libraryForAYear($library, ($year-1));
                $totalIssueFor1libraryFor2YearBefore = $em->getRepository('AppBundle:Issue')->totalIssueFor1libraryForAYear($library, ($year-2));


                return $this->render('AppBundle:Bibliovizz:page2.html.twig', array(
                    'year' => $year,
                    'library' => $library,
                    'myLibrary' => $myLibrary,
                    'totalBorrowerFor1library' => $totalBorrowerFor1libraryForAYear,
                    'totalBorrowerFor1libraryFor1YearBefore'=> $totalBorrowerFor1libraryFor1YearBefore,
                    'totalBorrowerFor1libraryFor2YearBefore' => $totalBorrowerFor1libraryFor2YearBefore,
                    'totalIssueFor1libraryForAYear' => $totalIssueFor1libraryForAYear,
                    'totalIssueFor1libraryFor1YearBefore' => $totalIssueFor1libraryFor1YearBefore,
                    'totalIssueFor1libraryFor2YearBefore' => $totalIssueFor1libraryFor2YearBefore,

                ));
                break;
            case 3:
                return $this->render('AppBundle:Bibliovizz:page3.html.twig', array(
                    'year' => $year,
                    'library' => $library
                ));

                break;
            case 4:
                return $this->render('AppBundle:Bibliovizz:page4.html.twig', array(
                    'year' => $year,
                    'library' => $library
                ));

                break;
            case 5:

                $nbEmprunteursByEtapeForALibrary = $em->getRepository('AppBundle:Issue')->nbEmprunteursByEtapeForALibrary($library, $year);
                $nbEmprunteursByNiveauForALibrary = $em->getRepository('AppBundle:Issue')->nbEmprunteursByNiveauForALibrary($library, $year);

                return $this->render('AppBundle:Bibliovizz:page5.html.twig', array(
                    'year' => $year,
                    'library' => $library,
                    'nbEmprunteursByEtapeForALibrary' => $nbEmprunteursByEtapeForALibrary,
                    'nbEmprunteursByNiveauForALibrary' =>$nbEmprunteursByNiveauForALibrary
                ));

                break;
            case 6:
                return $this->render('AppBundle:Bibliovizz:page6.html.twig', array(
                    'year' => $year,
                    'library' => $library
                ));

                break;
        }

    }


    /**
     * @Route("/bibliovizz/api/annee={year}&biblio={library}", name="api_bibliovizz")
     */
    public function apiYearAction($year, $library)
    {

        $em = $this->getDoctrine()->getManager();
        $nbIssueByDayForAYear = $em->getRepository('AppBundle:Issue')->nbIssueByDayForAYear($year, $library);

        return new JsonResponse($nbIssueByDayForAYear);
    }

    /**
     * @Route("/bibliovizz/api/chercheursVsPerson/annee={year}&biblio={library}", name="api_bibliovizz_chercheurs")
     */
    public function apiRatioChercheursEtud($year, $library){

        $em = $this->getDoctrine()->getManager();
        $totalEtudForAYear = $em->getRepository('AppBundle:Borrower')->totalEtudForAYear($library, $year);
        $totalChercheurForAYear = $em->getRepository('AppBundle:Borrower')->totalChercheurForAYear($library, $year);

        $response = [];
        $response = [$totalEtudForAYear, $totalChercheurForAYear];

        return new JsonResponse($response);
    }

    /**
     * @Route("/bibliovizz/api/etudByDiscipline/annee={year}&biblio={library}", name="api_bibliovizz_etud_discipline")
     */
    public function apiEtudDiscipline($year, $library){

        $em = $this->getDoctrine()->getManager();
        $nbEmprunteursByEtapeForALibrary = $em->getRepository('AppBundle:Issue')->nbEmprunteursByEtapeForALibrary($library, $year);

        return new JsonResponse($nbEmprunteursByEtapeForALibrary);
    }

    /**
     * @Route("/bibliovizz/api/etudByNiveau/annee={year}&biblio={library}", name="api_bibliovizz_etud_niveau")
     */
    public function apiEtudNiveau($year, $library){

        $em = $this->getDoctrine()->getManager();
        $nbEmprunteursByNiveauForALibrary = $em->getRepository('AppBundle:Issue')->nbEmprunteursByNiveauForALibrary($library, $year);

        return new JsonResponse($nbEmprunteursByNiveauForALibrary);
    }

}


