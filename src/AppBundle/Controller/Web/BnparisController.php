<?php

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BnparisController extends Controller
{
    /**
     * @Route("/paris/bn", name="paris_bn")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        //------ total
        $query = $em->createQuery("SELECT COUNT(s) as nb FROM AppBundle:StatsBNLogs s");
        $total = $query->getSingleResult();

        //------ nombre d'ouvrage
        $query = $em->createQuery("SELECT COUNT(DISTINCT s.title) as nbouvrage FROM AppBundle:StatsBNLogs s");
        $totalOuvrage = $query->getSingleResult();

        //------ nombre d'emprunteurs
        $query = $em->createQuery("SELECT COUNT(DISTINCT s.cb) as nbemprunteurs FROM AppBundle:StatsBNLogs s");
        $totalemprunteurs = $query->getSingleResult();

        //----- 10 classification
        $query = $em->createQuery("SELECT COUNT(s) as value ,s.classification as key FROM AppBundle:StatsBNLogs s WHERE s.classification != '' GROUP by key ORDER BY value DESC")->setMaxResults(10);
        $totalClassif = $query->getResult();


        //------- genre
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.gender as title FROM AppBundle:StatsBNLogs s GROUP BY s.gender");
        $genres = $query->getResult();

        //---- csp
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.csp as title FROM AppBundle:StatsBNLogs s WHERE s.csp != '' GROUP BY s.csp ORDER BY nb DESC");
        $csp = $query->getResult();


        //------ days
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.day as title FROM AppBundle:StatsBNLogs s GROUP BY s.day ORDER BY nb DESC");
        $days = $query->getResult();

        //------ hours
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.hour as title FROM AppBundle:StatsBNLogs s GROUP BY s.hour");
        $hours = $query->getResult();

        //------ birthday
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.birthday as title FROM AppBundle:StatsBNLogs s GROUP BY s.birthday");
        $birthdays = $query->getResult();

        //------ adresse
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.adresse as title FROM AppBundle:StatsBNLogs s GROUP BY s.adresse");
        $adresses = $query->getResult();

        //------ abonnement
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.abonnement as title FROM AppBundle:StatsBNLogs s GROUP BY s.abonnement");
        $abonnements = $query->getResult();

        //------ total d'abonnés
        $query = $em->createQuery("SELECT COUNT(DISTINCT(s.cb)) as nb FROM AppBundle:StatsBNLogs s");
        $total_cb = $query->getSingleResult();

        //------ top user
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.cb as title FROM AppBundle:StatsBNLogs s GROUP BY s.cb ORDER BY nb DESC")->setMaxResults(10);
        $top_users = $query->getResult();

        //------- top books
        $query = $em->createQuery("SELECT COUNT(s.ean) as nb, s.title as title, s.ean as ean, s.author as author, s.image as image FROM AppBundle:StatsBNlogs s GROUP BY s.ean ORDER BY nb DESC")->setMaxResults(8);
        $top_books = $query->getResult();

        //------- top 5 authors
        $query = $em->createQuery("SELECT COUNT(s.author)as nb, s.author as author FROM AppBundle:StatsBNlogs s GROUP BY s.author ORDER BY nb DESC")->setMaxResults(8);
        $top_authors = $query->getResult();

        //------- first and last date of logs
        $query = $em->createQuery("SELECT MAX(s.date) as last, MIN(s.date) as first FROM AppBundle:StatsBNlogs s");
        $dates = $query->getSingleResult();

        //------- nb de consultations par jour
        $query = $em->createQuery("SELECT s.date as day, COUNT(s) as nb FROM AppBundle:StatsBNlogs s GROUP by s.date ORDER BY s.date");
        $lendByDay = $query->getResult();

        //------- mois avec des emprunts

        $query = $em->createQuery("SELECT DISTINCT YEAR(s.date) as year FROM AppBundle:StatsBNlogs s");
        $year = $query->getResult();



        //--------- Classification
        $query = $em->createQuery("SELECT s.classification as key , COUNT(s) as value  FROM AppBundle:StatsBNLogs s WHERE s.classification != '' GROUP by key ORDER BY value DESC") -> setMaxResults(10);
        $classification = $query->getResult();

        foreach ($classification as $key => $item){
            $classification[$key]['value'] = (int)$classification[$key]['value'];
        }



        //---------- Couleurs donut
        $colors = ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf", "#fffb87"];



        return $this->render('AppBundle:Bnparis:index.html.twig', array(
            'total' => $total,
            'genres' => $genres,
            'csp' => $csp,
            'days' => $days,
            'hours' => $hours,
            'birthdays' => $birthdays,
            'adresses' => $adresses,
            'abonnements' => $abonnements,
            'total_cb' => $total_cb,
            'top_users' => $top_users,
            'top_books' => $top_books,
            'top_authors' => $top_authors,
            'dates' => $dates,
            'lendByDay' => $lendByDay,
            'year' => $year,
            'classification' => $classification,
            'totalouvrage' => $totalOuvrage,
            'totalemprunteurs' => $totalemprunteurs,
            'colors' => $colors,
            'totalClassif' => $totalClassif
        ));
    }

    /**
     * @Route("/paris/bn/book/{ean}", name="paris_bn_book")
     */
    public function bookAction($ean){

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.title as title, s.author as author, s.ean as ean, COUNT(s) as nb , s.classification as type FROM AppBundle:StatsBNLogs s WHERE s.ean = :ean GROUP BY s.ean");
        $query->setParameter('ean', $ean);
        $book = $query->getSingleResult();


        //---- nb de livres
        $query = $em->createQuery("SELECT COUNT(DISTINCT s.title) as value FROM AppBundle:StatsBNLogs s");
        $nbBooks = $query->getResult();
        $nbBooks = (int)$nbBooks[0]['value'];

        //---- cspLivre
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.csp as title FROM AppBundle:StatsBNLogs s  WHERE s.ean = :ean GROUP BY s.csp ORDER BY nb DESC");
        $query->setParameter('ean', $ean);
        $cspLivre = $query->getResult();
        foreach ($cspLivre as $key => $item){
            $cspLivre[$key]['nb'] = (int)$cspLivre[$key]['nb'];
        }


        //---- cspGeneral
        $query = $em->createQuery("SELECT  s.csp as label, COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%2017-%' AND s.csp != '' GROUP BY s.csp ORDER BY value DESC");
        $cspGeneral = $query->getResult();


        //------- genre
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.gender as title FROM AppBundle:StatsBNLogs s WHERE s.ean = :ean GROUP BY s.gender");
        $query->setParameter('ean', $ean);
        $genres = $query->getResult();

        $annee = date('Y');

        $query = $em->createQuery("SELECT AVG($annee - s.birthday) as value FROM AppBundle:StatsBNLogs s WHERE s.ean = :ean");
        $query->setParameter('ean', $ean);
        $ageMoyen = $query->getResult();

        foreach ($ageMoyen as $key => $item){
            $ageMoyen[$key]['value'] = (int)$ageMoyen[$key]['value'];
        }

        return $this->render('AppBundle:Bnparis:book.html.twig', array(
            'ean' => $ean,
            'book' => $book,
            'nbBooks' => $nbBooks,
            'cspLivre' => $cspLivre,
            'cspGeneral' => $cspGeneral,
            'genres' => $genres,
            'ageMoyen'=> $ageMoyen
        ));
    }

    /**
     * @Route("/paris/bn/author/{name}", name="paris_bn_author")
     */
    public function authorAction($name){

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.title as title, s.author as author, COUNT(s) as nb FROM AppBundle:StatsBNLogs s WHERE s.author = :auteur GROUP BY s.author");
        $query->setParameter('auteur', $name);
        $auteur = $query->getSingleResult();

        $query = $em->createQuery("SELECT s.title as title, s.ean as ean, s.author as author, COUNT(s.ean) as nb FROM AppBundle:StatsBNLogs s WHERE s.author = :auteur GROUP BY s.ean ORDER BY nb DESC");
        $query->setParameter('auteur', $name);
        $books = $query->getResult();

        $query = $em->createQuery("SELECT COUNT(s.title) as nb FROM AppBundle:StatsBNLogs s WHERE s.author = :auteur");
        $query->setParameter('auteur', $name);
        $prets = $query->getSingleResult();

        $query = $em->createQuery("SELECT (SUM(YEAR(CURRENT_DATE() )-s.birthday)/COUNT(s.birthday)) as age FROM AppBundle:StatsBNLogs s WHERE s.author = :auteur");
        $query->setParameter('auteur', $name);
        $query->setMaxResults(1);
        $age = $query->getSingleResult();

        $query = $em->createQuery("SELECT COUNT(s.ean) as nb FROM AppBundle:StatsBNLogs s WHERE s.author = :auteur GROUP BY s.ean ORDER BY nb ASC");
        $query->setParameter('auteur', $name);
        $query->setMaxResults(1);
        $min = $query->getSingleResult();

        $query = $em->createQuery("SELECT COUNT(s.ean) as nb FROM AppBundle:StatsBNLogs s WHERE s.author = :auteur GROUP BY s.ean ORDER BY nb DESC");
        $query->setParameter('auteur', $name);
        $query->setMaxResults(1);
        $max = $query->getSingleResult();

        return $this->render('AppBundle:Bnparis:auteur.html.twig', array(
            'auteur' => $auteur,
            'name' => $name,
            'books' => $books,
            'prets' => $prets,
            'age' => $age,
            'min' => $min,
            'max' => $max
        ));
    }


    /**
     * @Route("/paris/bn/genre/{genre}", name="paris_bn_genre")
     */
    public function genreAction($genre){


        $em = $this->getDoctrine()->getManager();

        //------- total
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.gender as title FROM AppBundle:StatsBNLogs s  WHERE s.gender = :genre GROUP BY s.gender");
        $query->setParameter("genre" , $genre);
        $total = $query->getSingleResult();

        //-------- CSP
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.csp as title FROM AppBundle:StatsBNLogs s  WHERE s.gender = :genre GROUP BY s.csp ORDER BY nb DESC");
        $query->setParameter("genre" , $genre);
        $csp = $query->getResult();

        //-------- heures
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.hour as title FROM AppBundle:StatsBNLogs s  WHERE s.gender = :genre GROUP BY s.hour ORDER BY title ASC");
        $query->setParameter("genre" , $genre);
        $hour = $query->getResult();

        //-------- adresse
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.adresse as title FROM AppBundle:StatsBNLogs s  WHERE s.gender = :genre GROUP BY s.adresse ORDER BY nb DESC");
        $query->setParameter("genre" , $genre);
        $adresses = $query->getResult();

        return $this->render('AppBundle:Bnparis:genre.html.twig', array(
            'total' => $total,
            'csp' => $csp,
            'hour' => $hour,
            'adresses' => $adresses
        ));

    }

    /**
     * @Route("/paris/bn/genre/{genre}/csp/{csp}", name="paris_bn_genre_csp")
     */
    public function genreCspAction($genre, $csp){

        $em = $this->getDoctrine()->getManager();

        //------- total
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.gender as title FROM AppBundle:StatsBNLogs s  WHERE s.gender = :genre AND s.csp = :csp GROUP BY s.gender");
        $query->setParameter("genre" , $genre);
        $query->setParameter("csp" , $csp);
        $total = $query->getSingleResult();

        //-------- heures
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.hour as title FROM AppBundle:StatsBNLogs s  WHERE s.gender = :genre AND s.csp = :csp GROUP BY s.hour ORDER BY title ASC");
        $query->setParameter("genre" , $genre);
        $query->setParameter("csp" , $csp);
        $hours = $query->getResult();

        //-------- days
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.day as title FROM AppBundle:StatsBNLogs s  WHERE s.gender = :genre AND s.csp = :csp GROUP BY s.day ORDER BY title ASC");
        $query->setParameter("genre" , $genre);
        $query->setParameter("csp" , $csp);
        $days = $query->getResult();

        //-------- adresse
        $query = $em->createQuery("SELECT COUNT(s) as nb, s.adresse as title FROM AppBundle:StatsBNLogs s  WHERE s.gender = :genre AND s.csp = :csp GROUP BY s.adresse ORDER BY nb DESC");
        $query->setParameter("genre" , $genre);
        $query->setParameter("csp" , $csp);
        $adresses = $query->getResult();

        return $this->render('AppBundle:Bnparis:genre_csp.html.twig', array(
            'total' => $total,
            'hours' => $hours,
            'days' => $days,
            'adresses' => $adresses
        ));
    }


    /**
     * @Route("/paris/bn/stats", name="paris_bn_stats")
     */
    public function statsAction(){

        $em = $this->getDoctrine()->getManager();

        //------ nombre d'ouvrage
        $query = $em->createQuery("SELECT COUNT(DISTINCT s.title) FROM AppBundle:StatsBNLogs s");
        $totalOuvrage = $query->getSingleResult();

        //------ total
        $query = $em->createQuery("SELECT COUNT(s) as nb FROM AppBundle:StatsBNLogs s");
        $total = $query->getSingleResult();

        //------ moyenne emprunt/jour

        $query = $em->createQuery("SELECT COUNT(DISTINCT s.date) FROM AppBundle:StatsBNLogs s");
        $nbjourtotal = $query->getSingleResult();


        $query = $em->createQuery("SELECT COUNT(s) FROM AppBundle:StatsBNLogs s");
        $nbemprunts = $query->getSingleResult();

        $moyenneempruntjour["moyenne"] = (int)$nbemprunts[1]/(int)$nbjourtotal[1];

        //-- max et min emprunt

        /* SELECT MAX(c) FROM (SELECT COUNT(cb) as c FROM `stats_bn_logs` GROUP BY date) as resu

        $query = $em->createQuery("SELECT MAX(value) as maximum FROM ( SELECT COUNT(s) as value FROM AppBundle:StatsBNLogs s GROUP BY s.date ) as r");
        $maxandmin = $query->getResult();*/

        $query = $em->createQuery("SELECT COUNT(s) as value FROM AppBundle:StatsBNLogs s GROUP BY s.date ORDER BY value");
        $maxandminjour = $query->getResult();

        $query = $em->createQuery("SELECT COUNT(s) as value, s.date as day1, s.hour as hour1 FROM AppBundle:StatsBNLogs s GROUP BY  s.date ,s.hour ORDER BY value DESC ");
        $maxandminheure = $query->getResult();


        $sql2 = $em->createQuery("SELECT COUNT(s) as value FROM AppBundle:StatsBNLogs s GROUP BY s.title");
        $allbook = $sql2->getResult();

        //-- Methode pas terrible pour récupérer le livres le plus et le moins empruntés
        $max = 0;

        foreach ($allbook as $key => $item){
            if($allbook[$key]['value'] > $max){
                $max = $allbook[$key]['value'];
            }
        }

        $min = $max;

        foreach ($allbook as $key => $item){
            if($allbook[$key]['value'] < $min){
                $min = $allbook[$key]['value'];
            }
        }

        $maxandminlivre = [2];
        $maxandminlivre[0]=$max;
        $maxandminlivre[1]=$min;

        // -- fin max min livre

        //------- first and last date of logs
        $query = $em->createQuery("SELECT MAX(s.date) as last, MIN(s.date) as first FROM AppBundle:StatsBNlogs s");
        $dates = $query->getSingleResult();

        // -- moyenne par heure

        $query = $em->createQuery("SELECT COUNT(DISTINCT s.hour) FROM AppBundle:StatsBNLogs s ");
        $hour = $query->getSingleResult();

       $moyenneempruntheure["moyenne"] = (int)$nbemprunts[1]/((int)$hour[1]*(int)$nbjourtotal[1]);

       //  combien de fois en moyenne un livre est emprunté

        $moyenneempruntlivre["moyenne"] = (int)$nbemprunts[1]/(int)$totalOuvrage[1];


        // Pour les emprunteurs
        // Moyenne d'age

        $temp = $em->createQuery("SELECT (YEAR(CURRENT_DATE()) - s.birthday) as value  FROM AppBundle:StatsBNLogs s WHERE s.birthday != '' GROUP BY s.cb");
        $temp1 = $temp->getResult();

        $result=0;
        $nbE = 0;
        foreach ($temp1 as $key => $item){
                $nbE++;
                $result += (int)$temp1[$key]['value'];
        }

       $moyennedage["value"] = $result/$nbE;


        // Moyenne d'emprunts par emprunteurs
        $query = $em->createQuery("SELECT COUNT(DISTINCT s.cb) FROM AppBundle:StatsBNLogs s");
        $nbemprunteurs = $query->getSingleResult();

        $moyenneempruntbyemprunteur["moyenne"] = (int)$nbemprunts[1]/(int)$nbemprunteurs[1];


        return $this->render('AppBundle:Bnparis:stats.html.twig', array(
            'total' => $total,
            'dates' => $dates,
            'moyenneempruntjour' => $moyenneempruntjour,
            'maxandminjour' => $maxandminjour,
            'moyenneempruntheure'=>$moyenneempruntheure,
            'maxandminheure' => $maxandminheure,
            'moyenneempruntlivre' => $moyenneempruntlivre,
            'maxandminlivre' => $maxandminlivre,
            'moyennedage' => $moyennedage,
            'moyenneempruntbyemprunteur' => $moyenneempruntbyemprunteur
        ));
    }

    /**
     * @Route("/paris/bn/stats", name="paris_bn_stats")
     */
    /*public function book_by_author($author){
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value , s.title as label FROM AppBundle:StatsBNLogs s WHERE s.author LIKE '$auteur%' GROUP BY s.title ORDER BY value DESC ")->setMaxResults(10);;
        $books = $query->getResult();

        return $this->render('AppBundle:Bnparis:stats.html.twig', array(
            'books_by_author' => $books
        ));
    }*/

    /**
     * @Route("/paris/bn/1fev", name="paris_bn_1fev")
     */
    public function fevAction(){

        $em = $this->getDoctrine()->getManager();

        //------ total
        $query = $em->createQuery("SELECT COUNT(s) as nb FROM AppBundle:StatsBNLogs s");
        $total = $query->getSingleResult();

        return $this->render('AppBundle:Bnparis:1fev.html.twig', array(
            'total' => $total
        ));
    }

    /**
     * @Route("/paris/bn/csp/{currentCsp}", name="paris_bn_csp")
     */
    public function bnCsp($currentCsp) {

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as nb, s.csp as title FROM AppBundle:StatsBNLogs s GROUP BY s.csp ORDER BY nb DESC");
        $csp = $query->getResult();

        $query = $em->createQuery("SELECT DISTINCT YEAR(s.date) as year FROM AppBundle:StatsBNlogs s");
        $year = $query->getResult();

        return $this->render('AppBundle:Bnparis:csp.html.twig', array(
            'csp' => $csp,
            'currentCsp' => $currentCsp,
            'year' => $year
        ));
    }

    /**
     * @Route("/paris/bn/statsGrosEmprunteurs", name="paris_bn_stats_gros_emprunteurs")
     */
    public function StatsGrosEmprunteurs() {
        $em = $this->getDoctrine()->getManager();

        //---- Liste total des emprunteurs
        $query = $em->createQuery("SELECT DISTINCT s.cb as total_emprunteurs FROM AppBundle:StatsBNLogs s GROUP BY s.cb");
        $liste_total_emprunteurs = $query->getResult();

        $nb_total_emprunteurs = 0;
        foreach ($liste_total_emprunteurs as $key => $item) {
            $liste_total_emprunteurs[$key]['total_emprunteurs'] = (int)$liste_total_emprunteurs[$key]['total_emprunteurs'];
            $nb_total_emprunteurs++;
        }

        //dump($liste_total_emprunteurs);die();
        //dump($nb_total_emprunteurs);die();

        //---- Nombre total d'emprunts
        $query = $em->createQuery("SELECT s.id as nb FROM AppBundle:StatsBNLogs s");
        $liste_total_emprunts = $query->getResult();

        $nb_total_emprunts = 0;
        foreach ($liste_total_emprunts as $key => $item) {
            $liste_total_emprunts[$key]['nb'] = (int)$liste_total_emprunts[$key]['nb'];
            $nb_total_emprunts++;
        }

        //dump($liste_total_emprunts);die();
        //dump($nb_total_emprunts);die();

        //---- Liste des plus gros emprunteurs
        $query = $em->createQuery("SELECT s.cb as emprunteur , (YEAR(CURRENT_DATE()) - s.birthday) as age , COUNT(s) as nb_emprunts FROM AppBundle:StatsBNLogs s GROUP BY s.cb HAVING nb_emprunts = 11 or nb_emprunts = 12 ORDER BY emprunteur");
        //dump($liste_total_emprunteurs);die();

        //dump($nb_total_emprunts);die();

        //---- Liste des plus gros emprunteurs (conditionner par le nombre de mois dont nous disposons plutôt que d'écrire 11 ou 12 en dur???)
        $query = $em->createQuery("SELECT s.cb as emprunteur , (YEAR(CURRENT_DATE()) - s.birthday) as age , s.csp as csp, COUNT(s) as nb_emprunts FROM AppBundle:StatsBNLogs s GROUP BY s.cb HAVING nb_emprunts = 11 or nb_emprunts = 12 ORDER BY emprunteur");
        $liste_gros_emrunteurs = $query->getResult();

        //dump($liste_gros_emrunteurs);die();

        $nb_gros_emprunteurs = 0;
        $ageTotal = 0;
        $nb_total_emprunts_gros_emprunteurs = 0;
        foreach ($liste_gros_emrunteurs as $key => $item) {
            $liste_gros_emrunteurs[$key]['nb_emprunts'] = (int)$liste_gros_emrunteurs[$key]['nb_emprunts'];
            $ageTotal += (int)$liste_gros_emrunteurs[$key]['age']; //---- Total des age des plus gros emprunteurs
            $nb_total_emprunts_gros_emprunteurs += (int)$liste_gros_emrunteurs[$key]['nb_emprunts'];
            $nb_gros_emprunteurs++; //---- Nb total des plus gros emprunteurs
        }

        //dump($nb_gros_emprunteurs);die();
        //dump($ageTotal);die();
        //dump($nb_total_emprunts_gros_emprunteurs);die();


        $query = $em->createQuery("SELECT s.cb, COUNT(s.cb) FROM AppBundle:StatsBNLogs s GROUP BY s.cb HAVING COUNT(s.cb) = 11 or COUNT(s.cb) = 12");
        $liste_gros_emrunteurs1 = $query->getResult();

        //dump($liste_gros_emrunteurs1);die();

        $emprunteurs = [];
        foreach ($liste_gros_emrunteurs1 as $key => $item) {
            array_push($emprunteurs, array("cb" => $liste_gros_emrunteurs1[$key]['cb']));
        }

        //dump($emprunteurs);die();

        $query = $em->createQuery("SELECT s.gender as genre, COUNT(s.cb) as nb_emprunteurs FROM AppBundle:StatsBNLogs s WHERE s.cb IN (:liste) GROUP BY s.gender");
        $query->setParameter('liste',$emprunteurs);
        $ratio_genre_gros_emprunteurs = $query->getResult();

        //dump($ratio_genre_gros_emprunteurs);die();

        //récupération des csp des gros emprunteurs
        $query = $em->createQuery("SELECT s.csp as csp, COUNT(s.cb) as nb_emprunteurs FROM AppBundle:StatsBNLogs s WHERE s.csp != '' AND s.cb IN (:liste) GROUP BY s.csp");
        $query->setParameter('liste',$emprunteurs);
        $ratio_csp_gros_emprunteurs = $query->getResult();


        //dump($ratio_csp_gros_emprunteurs);die();

        //dump($ratio_csp_gros_emprunteurs);die();


        return $this->render('AppBundle:Bnparis:stats_gros_emprunteurs.html.twig', array(
            'liste_gros_emrunteurs' => $liste_gros_emrunteurs,
            'nb_gros_emprunteurs' => $nb_gros_emprunteurs,
            'ageTotal' => $ageTotal,
            'liste_total_emprunteurs' => $liste_total_emprunteurs,
            'nb_total_emprunteurs' => $nb_total_emprunteurs,
            'nb_total_emprunts_gros_emprunteurs' => $nb_total_emprunts_gros_emprunteurs,
            'ratio_genre_gros_emprunteurs' => $ratio_genre_gros_emprunteurs,
            'ratio_csp_gros_emprunteurs' => $ratio_csp_gros_emprunteurs,
            'nb_total_emprunts' => $nb_total_emprunts
        ));
    }


    /**
     * @Route("/paris/bn/recherche/livre", name="paris_bn_search_livre")
     */
    public function searchBookAction() {

        return $this->render('AppBundle:Bnparis:search_livre.html.twig', array(

        ));

    }

    /**
     * @Route("/paris/bn/recherche/auteur", name="paris_bn_search_auteur")
     */
    public function searchAuteurAction(){

        return $this->render('AppBundle:Bnparis:search_auteur.html.twig', array(

        ));

    }
}

//Pour l'import des données biblio de Paris
//cb, localisation, abonnement, birthday, gender, csp, adresse, ean, title, author, classification, date, day_short, hour