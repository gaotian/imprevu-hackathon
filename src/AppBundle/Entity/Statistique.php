<?php
/**
 * Created by PhpStorm.
 * User: gaetan
 * Date: 17/06/2017
 * Time: 12:53
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Book
 *
 * @ORM\Table(name="stats")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatsRepository")
 */
class Statistique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_stats", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idStats;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, unique=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="books", type="integer", length=11, nullable=true)
     */
    private $books;

    /**
     * @var string
     *
     * @ORM\Column(name="items", type="integer", length=11, nullable=true)
     */
    private $items;

    /**
     * @var string
     *
     * @ORM\Column(name="issues", type="integer", length=11, nullable=true)
     */
    private $issues;

    /**
     * @var string
     *
     * @ORM\Column(name="borrowers", type="integer", length=11, nullable=true)
     */
    private $borrowers;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_creation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_update;

    /**
     * @return int
     */
    public function getIdStats()
    {
        return $this->idStats;
    }

    /**
     * @param int $idStats
     */
    public function setIdStats($idStats)
    {
        $this->idStats = $idStats;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * @param string $books
     */
    public function setBooks($books)
    {
        $this->books = $books;
    }

    /**
     * @return string
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param string $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return string
     */
    public function getIssues()
    {
        return $this->issues;
    }

    /**
     * @param string $issues
     */
    public function setIssues($issues)
    {
        $this->issues = $issues;
    }

    /**
     * @return string
     */
    public function getBorrowers()
    {
        return $this->borrowers;
    }

    /**
     * @param string $borrowers
     */
    public function setBorrowers($borrowers)
    {
        $this->borrowers = $borrowers;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @return mixed
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * @param mixed $last_update
     */
    public function setLastUpdate($last_update)
    {
        $this->last_update = $last_update;
    }

}