<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TempBNLogs
 *
 * @ORM\Table(name="tempBNLogs")
 * @ORM\Entity()
 */
class TempBNLogs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cb", type="string", length=45, nullable=true)
     */
    private $cb;

    private $bibliotheque;

    private $abonnement;

    private $birthday;

    private $gender;

    private $csp;

    private $quartier;

    private $ean;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=500, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=500, nullable=true)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="classification", type="string", length=500, nullable=true)
     */
    private $classification;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="hour", type="integer", length=2, nullable=true)
     */
    private $hour;

    /**
     * @var string
     *
     * @ORM\Column(name="day", type="string", length=45, nullable=true)
     */
    private $day;

    /**
     * @var string
     *
     * @ORM\Column(name="day_short", type="integer", length=1, nullable=true)
     */
    private $day_short;



}