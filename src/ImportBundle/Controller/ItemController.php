<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ItemController extends Controller
{
    public function insert($db_name, $base, $library){

        $user = $this->container->getParameter('database_user');
        $password = $this->container->getParameter('database_password');
        $host = $this->container->getParameter('database_host');

        $connectionParamsPrevu = array(
            'dbname' => 'prevu',
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $connectionParams = array(
            'dbname' => $db_name,
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $config = new \Doctrine\DBAL\Configuration();
        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        $connPrevu = \Doctrine\DBAL\DriverManager::getConnection($connectionParamsPrevu, $config);

        //ajout des items
        $sql = "INSERT INTO prevu.item (`id_book`, `price`, koha, library, `date_creation`, `last_update`) (SELECT p.prevu,price, i.itemnumber,:library, NOW(), NOW() FROM ".$base.".items as i INNER JOIN prevu.association as p ON i.biblionumber = p.koha WHERE p.library = :library)";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("library", $library);
        $stmt->execute();

    }

    /**
     * @Route("/import/items/lib={library}", name="import_items")
     */
    public function importBorrowersAction($library)
    {


        switch ($library) {
            //Base de données Koha de Paris 8
            case "up8":
                $dbname = $this->container->getParameter('database_name2');

                $library = 1;

                $this->insert($dbname, "koha", $library);


                break;

            //Base de données Koha de Roubaix
            case "rbx":
                $dbname = $this->container->getParameter('database_name3');

                $library = 2;

                $this->insert($dbname, "prevu_rbx", $library);

            //Base de données Koha de Saclay
            case "scl":
                $dbname = $this->container->getParameter('database_name4');

                $library = 3;

                $this->insert($dbname, "prevu_saclay", $library);

                break;
            default :
                return $this->render('ImportBundle:Book:index.html.twig');
                break;
        }
        return $this->render('ImportBundle:Default:index.html.twig');
    }
}
