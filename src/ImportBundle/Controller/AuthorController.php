<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AuthorController extends Controller
{
    public function insert($db_name, $base, $library){

        $user = $this->container->getParameter('database_user');
        $password = $this->container->getParameter('database_password');
        $host = $this->container->getParameter('database_host');

        $connectionParamsPrevu = array(
            'dbname' => 'prevu',
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $connectionParams = array(
            'dbname' => $db_name,
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $config = new \Doctrine\DBAL\Configuration();
        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        $connPrevu = \Doctrine\DBAL\DriverManager::getConnection($connectionParamsPrevu, $config);

        //Créer une table intermédiaire
//
//        $sql= "CREATE TABLE import_author (id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,firstname VARCHAR(500) NOT NULL,lastname VARCHAR(500) NOT NULL, dates VARCHAR(45), id_book INT(11), library INT(11), id_objet(11)";
//        $stmt = $connPrevu->prepare($sql);
//        $stmt->execute();

        //Insertion des données de koha books
        $sql = "INSERT INTO `import_author`(firstname, lastname, dates, id_book, library) (SELECT EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"a\"]') as lastname, EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"b\"]') as firstname,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"f\"]') as dates, biblionumber, :library FROM koha.biblioitems)";
        $stmt = $connPrevu->prepare($sql);
        $stmt->bindValue("library", $library);
        $stmt->execute();

        //Ensuite insérer tous les auteurs dans la table auteur


        //Updater les doublons
        $sql = "SELECT COUNT(*) as nb, MIN(id) as id, firstname, lastname, dates FROM import_author GROUP BY firstname, lastname, dates";
    }

    /**
     * @Route("/import/authors/lib={library}", name="import_authors")
     */
    public function importBorrowersAction($library)
    {


        switch ($library) {
            //Base de données Koha de Paris 8
            case "up8":
                $dbname = $this->container->getParameter('database_name2');

                $library = 1;

                $this->insert($dbname, "koha", $library);


                break;

            //Base de données Koha de Roubaix
            case "rbx":
                $dbname = $this->container->getParameter('database_name3');

                $library = 2;

                $this->insert($dbname, "prevu_rbx", $library);

            //Base de données Koha de Saclay
            case "scl":
                $dbname = $this->container->getParameter('database_name4');

                $library = 3;

                $this->insert($dbname, "prevu_saclay", $library);

                break;
            default :
                return $this->render('ImportBundle:Book:index.html.twig');
                break;
        }
        return $this->render('ImportBundle:Default:index.html.twig');
    }
}
