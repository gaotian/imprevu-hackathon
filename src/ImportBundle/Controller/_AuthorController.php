<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AuthorController extends Controller
{
    public function indexAction($name)
    {

        //Insert des nouveaux auteurs

        $sql_author = "SELECT EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"a\"]') as lastname, EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"b\"]') as firstname,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"f\"]') as dates FROM biblioitems WHERE biblionumber > :id LIMIT 1;";
        $stmt = $conn->prepare($sql_author);
        $stmt->bindValue("id", $last_id);
        $stmt->execute();
        $author = $stmt->fetch();

        $now = microtime(true) - $item_time;
        echo "<p>Durée après sélection des auteurs = ".$now." secondes.</p>";

//                                $firstname = utf8_encode(addslashes($author['firstname']));
//                                $lastname = utf8_encode(addslashes($author['lastname']));
//                                $dates = utf8_encode(addslashes($author['dates']));

        if(ISSET($author['firstname'])){
            $firstname = utf8_encode(addslashes($author['firstname']));
        }
        else{
            $firstname = '';
        }

        if(ISSET($author['lastname'])){
            $lastname = utf8_encode(addslashes($author['lastname']));
        }
        else{
            $lastname = '';
        }

        if(ISSET($author['dates'])){
            $dates = utf8_encode(addslashes($author['dates']));
        }
        else{
            $dates = '';
        }

        $now = microtime(true) - $item_time;
        echo "<p>Durée après encodage info  auteurs = ".$now." secondes.</p>";

        //on vérifie si l'auteur existe déjà dans Prévu
        $sql_check = "SELECT COUNT(*) as nb FROM author WHERE short_firstname = :firstname AND short_lastname = :lastname AND dates = :dates ";
        $stmt = $connPrevu->prepare($sql_check);
        $stmt->bindValue("firstname", $firstname);
        $stmt->bindValue("lastname", $lastname);
        $stmt->bindValue("dates", $dates);
        $stmt->execute();
        $checkAuthor = $stmt->fetch();
        $checkAuthor = $checkAuthor['nb'];

        $now = microtime(true) - $item_time;
        echo "<p>Durée après vérification doublons  auteurs = ".$now." secondes.</p>";

//                                $now = microtime(true) - $item_time;
//                                echo "<p>Durée après vérification de l'existence de l'auteur = ".$now." secondes.</p>";

        //Si l'auteur n'existe pas déjà, on insère ses infos
        if ($checkAuthor < 1) {

            $sql = "INSERT INTO author (firstname, short_firstname, lastname, short_lastname, dates, date_creation, last_update) VALUES(:firstname, :firstname, :lastname, :lastname, :dates,NOW(), NOW() )";
            $stmt = $connPrevu->prepare($sql);
            $stmt->bindValue('firstname', $firstname);
            $stmt->bindValue("lastname", $lastname);
            $stmt->bindValue("dates", $dates);
            $stmt->execute();

            $sql = "SELECT id_author as id FROM author ORDER by id_author DESC LIMIT 1";
            $id_author = $connPrevu->fetchAssoc($sql);
            $id_author = $id_author['id'];

        } //Si l'auteur existe, on récupère son id
        else {
            $sql_check = "SELECT id_author as id FROM author WHERE firstname = :firstname AND lastname = :lastname AND dates = :dates LIMIT 1";
            $stmt = $connPrevu->prepare($sql_check);
            $stmt->bindValue("firstname", $firstname);
            $stmt->bindValue("lastname", $lastname);
            $stmt->bindValue("dates", $dates);
            $stmt->execute();
            $id_author = $stmt->fetch();
            $id_author = $id_author['id'];
        }

        $now = microtime(true) - $item_time;
        echo "<p>Durée juste avant l'insert de l'auteur = ".$now." secondes.</p>";

        return $this->render('', array('name' => $name));
    }
}


//-----------------------------------------------------------
//Ajout des auteurs secondaires : relation book / author many to many dans table intermédiaire - TODO
//-----------------------------------------------------------


//to do récupérer plusieurs auteurs = utiliser cette requête et enlever l'autre, ou alors la déplacer ailleurs
//                                $sql_author = "SELECT EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"a\"]') as lastname1, EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"b\"]') as firstname1,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"f\"]') as dates1, EXTRACTVALUE(marcxml,'//datafield[@tag=\"701\"]/subfield[@code=\"a\"]') as lastname2, EXTRACTVALUE(marcxml,'//datafield[@tag=\"701\"]/subfield[@code=\"b\"]') as firstname2,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"701\"]/subfield[@code=\"f\"]') as dates2  FROM biblioitems WHERE biblionumber > :id LIMIT 1;";

//si la notice n'existait pas déjà, on ajoute l'auteur
//if($exist < 1){
//    //-----------------------------------------------------------
//    //Ajout du mains auteur : relation book / author dans book
//    //-----------------------------------------------------------
//    $sql = "UPDATE book SET id_author = :author WHERE id_book = :book";
//    $stmt = $connPrevu->prepare($sql);
//    $stmt->bindValue("author", $id_author);
//    $stmt->bindValue("book", $id_prevu);
//    $stmt->execute();
//}


//            $sql2 ="CREATE INDEX `index_author` ON `author`(`dates`, `short_firstname`, `short_lastname`)";