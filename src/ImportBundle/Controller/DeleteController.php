<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * @Route("/import/delete")
 */
class DeleteController extends Controller
{
    //effacer toutes les données d'une bibliothèque


    /**
     * @Route("/library/{id}/items", name="import_delete")
     */
    public function deleteAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        //Suppressions des associations (mais on garde les notices)

        //select all books for a library
        $query = $em->createQuery("SELECT k FROM AppBundle:Key k JOIN k.library l WHERE l.idlibrary = :id");
        $query->setParameter('id', $id);
        $keys = $query->getResult();

        $query = $em->createQuery("DELETE FROM AppBundle:Key k WHERE k.idKey IN (:books)");
        $query->setParameter('books', $keys);
        $query->execute();


        //Suppressions des borrowers?

        $this->addFlash(
            'success',
            'Associations bien supprimées'
        );


        return $this->redirectToRoute('import');

        //DELETE ALL BOOKS

    }
}
