<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BorrowerController extends Controller
{

    public function insert($db_name, $base, $library){

        $user = $this->container->getParameter('database_user');
        $password = $this->container->getParameter('database_password');
        $host = $this->container->getParameter('database_host');

        $connectionParamsPrevu = array(
            'dbname' => 'prevu',
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $connectionParams = array(
            'dbname' => $db_name,
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $config = new \Doctrine\DBAL\Configuration();
        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        $connPrevu = \Doctrine\DBAL\DriverManager::getConnection($connectionParamsPrevu, $config);

        //Il faudrait ajouter des vérifications

//        Ajout des borrowers
        $sql = "INSERT INTO prevu.borrower(`koha`, `yearofbirth`,`library`, `date_creation`, `last_update`) (SELECT ".$base.".borrowers.borrowernumber, YEAR(dateofbirth), :library, NOW(), NOW() FROM ".$base.".borrowers)"; //les borrowers ont une libary seulement (permet de supprimer
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("library", $library);
        $stmt->execute();
//
//        //pre-calcul du numbre de prêt par livre
//        $sql ="UPDATE `book` SET `issues`=  (SELECT COUNT(*) FROM issue WHERE issue.id_book = book.id_book GROUP BY id_book WHERE i.id_library = :library)";
//        $stmt = $connPrevu->prepare($sql);
//        $stmt->bindValue("library", $library);
//        $stmt->execute();
    }


    /**
     * @Route("/import/borrowers/lib={library}", name="import_borrowers")
     */
    public function importBorrowersAction($library)
    {


        switch ($library) {
            //Base de données Koha de Paris 8
            case "up8":
                $dbname = $this->container->getParameter('database_name2');

                $library = 1;

                $this->insert($dbname, "koha", $library);


                break;

            //Base de données Koha de Roubaix
            case "rbx":
                $dbname = $this->container->getParameter('database_name3');

                $library = 2;

                $this->insert($dbname, "prevu_rbx", $library);

            //Base de données Koha de Saclay
            case "scl":
                $dbname = $this->container->getParameter('database_name4');

                $library = 3;

                $this->insert($dbname, "prevu_saclay", $library);

                break;
            default :
                return $this->render('ImportBundle:Book:index.html.twig');
                break;
        }


        //Update des id borrowers

        //création d'index
        $sql = "CREATE INDEX index_borrower ON borrower (id_borrower)";
        $sql = "CREATE INDEX index_borrower ON borrower (`koha`)";
        $sql = "CREATE INDEX index_borrower ON issue (id_borrower)";
        $sql = "CREATE INDEX index_kohaborrower ON issue (koha_borrower)";
        $sql = "CREATE INDEX index_kohaitem ON issue (koha_item)";

        //Update des id_books

        $sql = "UPDATE issue i JOIN borrower b ON b.koha = i.koha_borrower SET i.id_borrower= b.id_borrower WHERE i.id_library = 1";
        //Rbx (ok avec index)
        $sql = "UPDATE issue i JOIN borrower b ON b.koha = i.koha_borrower SET i.id_borrower= b.id_borrower WHERE i.id_library = 2";


        //Update des id notices
        $sql = "CREATE INDEX index_item ON item (koha)";
        $sql = "CREATE INDEX index_item ON issue (koha_item)";

        $sql = "SELECT i.id_book FROM issue i LEFT JOIN item t ON t.koha = i.koha_item";
        $sql = "UPDATE issue i JOIN item t ON t.koha = i.koha_item SET i.id_book = t.id_book WHERE i.id_library = 1";
        //rbx - 10h21 à
        $sql = "UPDATE issue i JOIN item t ON t.koha = i.koha_item SET i.id_book = t.id_book WHERE i.id_library = 2";

        //update des prêts pour les issues
        $sql = "SELECT b.title, COUNT(*) as nb FROM book as b INNER JOIN issue as i ON i.id_book = b.id_book GROUP BY b.id_book ORDER BY nb DESC";

        $sql ="UPDATE `book` SET `issues`=  (SELECT COUNT(*) FROM issue WHERE issue.id_book = book.id_book GROUP BY id_book );"; //ajouter where library

        return $this->render('ImportBundle:Default:index.html.twig');
    }

    /**
     * @Route("/import/borrowers/delete/lib={library}", name="import_borrowers_delete")
     */
    public function DeleteBorrowersAction($library){

        $em = $this->getDoctrine()->getManager();

        //------ total
        $query = $em->createQuery("DELETE FROM AppBundle:Borrower");
        $query->execute();

        return $this->render('ImportBundle:Default:index.html.twig');
    }
}