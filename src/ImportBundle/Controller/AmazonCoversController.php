<?php
/**
 * Created by PhpStorm.
 * User: melvi
 * Date: 02/05/2017
 * Time: 16:22
 */

namespace ImportBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class AmazonCoversController
{

    /**
     * @Route("/import/covers", name="import_covers")
     */
    public function ImportBooksAction(){

        $book=array("isbn_13"=>'', "id_worldcat"=>'', "id_google"=>'','ASIN'=>'', 'cover'=>'', 'title'=>'', 'authors'=>'', "description"=>'', "publisher"=>'', 'date'=>'', "numberOfPages"=>0,'nb_book'=>0,'url'=>'');
//$q = urlencode(htmlentities($_GET['q']));
        $q = urlencode(htmlentities("2070196674"));

        $q = "2070196674";


        //signature : PVTj3HG7BGb%2FgSaO2Su7smch40dSZGzHIEjcoUXjN9s%3D

        $private_key = "TcXgTRxb53qFsAJODmcWlE9vG3F/ixtIoHI60yqE";
        $params["AWSAccessKeyId"] = "AKIAJUGUA4J2FKOM2E6Q";
        $params["AssociateTag"] = "tactiquesorg-21";
//        $private_key = "wUI5Vm6ScBj9Ag30Wq0NAt2A7LdaYIxJV/wMFbIl";
//        $params["AWSAccessKeyId"] = "AKIAJU3PYLPYGNMP7S6Q";
//        $params["AssociateTag"] = "prevufr-21";


// GMT timestamp
        $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');




        function signAmazonUrl($url, $secret_key)
        {
            $original_url = $url;
            // Decode anything already encoded
            $url = urldecode($url);
            // Parse the URL into $urlparts
            $urlparts       = parse_url($url);
            // Build $params with each name/value pair
            foreach (explode("&", $urlparts['query']) as $part) {
                if (strpos($part, '=')) {
                    list($name, $value) = explode('=', $part, 2);
                } else {
                    $name = $part;
                    $value = '';
                }
                $params[$name] = $value;
            }
            // Include a timestamp if none was provided
            if (empty($params['Timestamp'])) {
                $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');
            }
            // Sort the array by key
            ksort($params);
            // Build the canonical query string
            $canonical       = '';
            foreach ($params as $key => $val) {
                $canonical  .= "$key=".rawurlencode(utf8_encode($val))."&";
            }
            // Remove the trailing ampersand
            $canonical       = preg_replace("/&$/", '', $canonical);
            // Some common replacements and ones that Amazon specifically mentions
            $canonical       = str_replace(array(' ', '+', ',', ';'), array('%20', '%20', urlencode(','), urlencode(':')), $canonical);
            // Build the sign
            $string_to_sign             = "GET\n{$urlparts['host']}\n{$urlparts['path']}\n$canonical";
            // Calculate our actual signature and base64 encode it
            $signature            = base64_encode(hash_hmac('sha256', $string_to_sign, $secret_key, true));
            // Finally re-build the URL with the proper string and include the Signature
            $url = "{$urlparts['scheme']}://{$urlparts['host']}{$urlparts['path']}?$canonical&Signature=".rawurlencode($signature);
            return $url;
        }

        $amazon_request=signAmazonUrl('http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&Operation=ItemLookup&ResponseGroup=Large&SearchIndex=All&IdType=ISBN&ItemId='.$q.'&AWSAccessKeyId='.$params["AWSAccessKeyId"].'&AssociateTag='.$params["AssociateTag"].'&Timestamp='.$params["Timestamp"], $private_key);

        //$amazon_request="http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=AKIAJU3PYLPYGNMP7S6Q&AssociateTag=prevufr-21&Operation=ItemLookup&ItemId=B00008OE6I&Timestamp=".gmdate('Y-m-d\TH:i:s\Z');


        $xml = simplexml_load_file($amazon_request); //retrieve URL and parse XML content
        $book['url']=$amazon_request;
        $book['ASIN']=(string) $xml->Items->Item->ASIN;
        $book['cover']=(string) $xml->Items->Item->MediumImage->URL;
        $book['isbn_13']=(string) $xml->Items->Item->ItemAttributes->EAN;
        if(strlen((string) $xml->Items->Item->ItemAttributes->Publisher)>0){
            //Manufacturer Label
            $book['publisher']=(string) $xml->Items->Item->ItemAttributes->Publisher;
        } else if(strlen((string) $xml->Items->Item->ItemAttributes->Manufacturer)>0){
            $book['publisher']=(string) $xml->Items->Item->ItemAttributes->Manufacturer;
        } else if(strlen((string) $xml->Items->Item->ItemAttributes->Label)>0){
            $book['publisher']=(string) $xml->Items->Item->ItemAttributes->Label;
        }

        $book['title']=(string) $xml->Items->Item->ItemAttributes->Title;
        $book['authors']=(string) $xml->Items->Item->ItemAttributes->Author;

        if(strlen($book['isbn_13'])>=10){
            $book['nb_book']=1;
        }

        return json_encode($book);

    }

}