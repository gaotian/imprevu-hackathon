<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BookController extends Controller
{

    //---------------------------
    //----Import all books ------
    //---------------------------


    //----- pb encodage du nom des auteurs à corriger / ajouter un role pour les auteurs
    // --- limiter la longueur des auteurs à 255 caractères (aucune ne dépasse 100 dans Roubaix), supprimer les shorts names et créer les index pour les names

    /**
     * @Route("/import/books/lib={biblio}", name="import_books")
     */
    public function ImportBooksAction($biblio){

//        echo microtime(true);
        $start_time = microtime(true);
        echo "<article><h1>Lancement de la fonction</h1><p> Lauchtime = ".$start_time." ; ";

        $user = $this->container->getParameter('database_user');
        $password = $this->container->getParameter('database_password');
        $host = $this->container->getParameter('database_host');

        switch ($biblio) {
            //Base de données Koha de Paris 8
            case "up8":
                $dbname = $this->container->getParameter('database_name2');

                $connectionParams = array(
                    'dbname' => $dbname,
                    'user' => $user,
                    'password' => $password,
                    'host' => $host,
                    'driver' => 'pdo_mysql',
                );
                $library = 1;
                break;

            //Base de données Koha de Roubaix
            case "rbx":
                $dbname = $this->container->getParameter('database_name3');
                $connectionParams = array(
                    'dbname' => $dbname,
                    'user' => $user,
                    'password' => $password,
                    'host' => $host,
                    'driver' => 'pdo_mysql',
                );
                $library = 2;
                break;

            //Base de données Koha de Saclay
            case "scl":
                $dbname = $this->container->getParameter('database_name4');
                $connectionParams = array(
                    'dbname' => $dbname,
                    'user' => $user,
                    'password' => $password,
                    'host' => $host,
                    'driver' => 'pdo_mysql',
                );
                $library = 3;
            //Resta à ajouter Rennes
                break;
            default :
                return $this->render('ImportBundle:Book:index.html.twig');
                break;
        }

        //Base de données de Prévu
        $connectionParamsPrevu = array(
            'dbname' => 'prevu',
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $config = new \Doctrine\DBAL\Configuration();
        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        $connPrevu = \Doctrine\DBAL\DriverManager::getConnection($connectionParamsPrevu, $config);

        $sql_total = "SELECT COUNT(*)as nb FROM biblio";
        $totalBooks = $conn->fetchAssoc($sql_total); //nb total de livres dans la library d'origine

        $count = $totalBooks; //nb de requêtes par session
//        $count = 10000; //pour les tests

        $now = microtime(true) - $start_time;
        echo "Just before launch one item = ".$now." .</p></article>";


        for ($i = 0; $i< $count ;$i++) {

            $item_time = microtime(true);
            echo "Lancement de la save d'un item = ".$item_time." .</p></article>";


            //-----------------------------------------------------------
            //Vérification des doublons pour cette bibliothèque
            //-----------------------------------------------------------

            //Sélection du dernier biblionumber qu'on a enregistré dans Prévu

            $sql_koha = "SELECT koha as id FROM association WHERE library = :library ORDER BY id_key DESC LIMIT 1"; //0.0002s
            $stmt = $connPrevu->prepare($sql_koha);
            $stmt->bindValue("library", $library);
            $stmt->execute();
            $lastKoha = $stmt->fetch(); //dernier id de koha sauvegardé dans key
            $last_id = $lastKoha['id'];

            //Vérification qu'il ne s'agit pas de la première notice qu'on insère, si c'est le cas, on crée un faux last id à 0

            if ($last_id < 1) {
                $last_id = 0;
            }

            //Nous récupérons le nombre de notices restantes à sauvegarder (celles dont l'id est plus grand que le dernier id koha sauvegardé pour cette bibliothèque

            $sql = "SELECT biblionumber as id FROM biblio WHERE biblionumber > :id LIMIT 1"; // 0.0002s
            $stmt = $conn->prepare($sql);
            $stmt->bindValue("id", $last_id);
            $stmt->execute();
            $id_koha = $stmt->fetch();
            $id_koha = $id_koha['id'];

            //S'il n'y pas d'id_koha, c'est parce que nous arrivons à la fin de l'import (il n'y a pas de plus grand biblionumber)
            if ($id_koha == null) {
                $this->addFlash('success', "L'import a déjà été effectué");
                break;
            }

            else
            {

                //Création de l'id_book pour prévu
                $now = microtime(true) - $item_time;
                echo "Lancement de la création de la notice : ".$now." secondes ; ";

                $sql = "SELECT MAX(id_book) as id FROM prevu.book"; // 0.0004 sec
                $id_prevu = $conn->fetchAssoc($sql);
                $id_prevu = $id_prevu['id'] + 1;

                //-----------------------------------------------------------
                //Vérification pour voir si on a déjà entré cette donnée
                //-----------------------------------------------------------

                //On compte le nombre de notices qui ont déjà ce code pour savoir si nous possédons déjà cette notice
                $sql = "SELECT COUNT(*) as nb FROM prevu.association WHERE koha = :id AND library = :library ";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue("id", $id_koha);
                $stmt->bindValue("library", $library);
                $stmt->execute();
                $check = $stmt->fetch();
                $check = $check['nb'];

//                $check = 0;

                $now = microtime(true) - $item_time;
                echo "after check doublon : ".$now." secondes ; ";

                //si check est plus petit que 1, cela signifie que la notice n'a pas encore été entrée pour cette bibliothèque
                if ($check < 1) {

//                    dump($count);die;
                    //-----------------------------------------------------------------------------------------------------------------
                    //Vérification si la notice existe déjà dans la base pour une autre bibiliothèque cette fois
                    //-----------------------------------------------------------------------------------------------------------------

                    //Nous récupérons le titre et l'isbn du document

                    $sql = "SELECT b.title as title, i.isbn as isbn, i.publicationyear as publication FROM biblioitems as i INNER JOIN biblio as b ON b.biblionumber = i.biblionumber WHERE b.biblionumber = :id ";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue("id", $id_koha);
                    $stmt->execute();
                    $ref = $stmt->fetch();

                    //si un livre dans une autre bibliothèque mais dans prevu a un titre, un auteur, une année de publication et un isbn, on récupère sa son id KOHA

//                    $title = utf8_encode($ref['title']);
//                    $isbn = utf8_encode(addslashes($ref['isbn']));
                    $title = $ref['title'];
                    $isbn = $ref['isbn'];
                    $publication = $ref['publication'];

                    $now = microtime(true) - $item_time;
                    echo "Extractions titres et isbn : ".$now." secondes ; ";

//                    dump($title);die;

                    //optimiser cette partie
                    //On compte le nombre de notices dans book de prévu qui existent déjà
                    $sql = "SELECT COUNT(b.id_book) as nb FROM book as b  WHERE  b.isbn = :isbn AND b.publicationyear = :publication AND b.short_title = :title";
                    $stmt = $connPrevu->prepare($sql);
                    $stmt->bindValue("title", $title);
                    $stmt->bindValue("isbn", $isbn);
                    $stmt->bindValue("publication", $publication);
                    $stmt->execute();
                    $exist = $stmt->fetch();

//                    dump($exist);

                    $exist = $exist['nb'];

                    $now = microtime(true) - $item_time;
                    echo "Si il y a doublon, on regarde ici : ".$now." secondes ; ";

//                    dump($isbn.$title.$exist);die;

                    //Si exist = 0, cela veut dire que le livre n'existe pas déjà dans prévu
                    if ($exist < 1) {

                        //-----------------------------------------------------------
                        //Création de la notice
                        //-----------------------------------------------------------

                        switch ($library) {

                            //UP8
                            case 1:

                                //Insert des nouveaux auteurs

                                $sql_author = "SELECT EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"a\"]') as lastname, EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"b\"]') as firstname,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"f\"]') as dates FROM biblioitems WHERE biblionumber > :id LIMIT 1;";
                                $stmt = $conn->prepare($sql_author);
                                $stmt->bindValue("id", $last_id);
                                $stmt->execute();
                                $author = $stmt->fetch();

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée après sélection des auteurs = ".$now." secondes.</p>";

//                                $firstname = utf8_encode(addslashes($author['firstname']));
//                                $lastname = utf8_encode(addslashes($author['lastname']));
//                                $dates = utf8_encode(addslashes($author['dates']));

                                if(ISSET($author['firstname'])){
                                    $firstname = utf8_encode(addslashes($author['firstname']));
                                }
                                else{
                                    $firstname = '';
                                }

                                if(ISSET($author['lastname'])){
                                    $lastname = utf8_encode(addslashes($author['lastname']));
                                }
                                else{
                                    $lastname = '';
                                }

                                if(ISSET($author['dates'])){
                                    $dates = utf8_encode(addslashes($author['dates']));
                                }
                                else{
                                    $dates = '';
                                }

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée après encodage info  auteurs = ".$now." secondes.</p>";

                                //on vérifie si l'auteur existe déjà dans Prévu
                                $sql_check = "SELECT COUNT(*) as nb FROM author WHERE short_firstname = :firstname AND short_lastname = :lastname AND dates = :dates ";
                                $stmt = $connPrevu->prepare($sql_check);
                                $stmt->bindValue("firstname", $firstname);
                                $stmt->bindValue("lastname", $lastname);
                                $stmt->bindValue("dates", $dates);
                                $stmt->execute();
                                $checkAuthor = $stmt->fetch();
                                $checkAuthor = $checkAuthor['nb'];

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée après vérification doublons  auteurs = ".$now." secondes.</p>";

//                                $now = microtime(true) - $item_time;
//                                echo "<p>Durée après vérification de l'existence de l'auteur = ".$now." secondes.</p>";

                                //Si l'auteur n'existe pas déjà, on insère ses infos
                                if ($checkAuthor < 1) {

                                    $sql = "INSERT INTO author (firstname, short_firstname, lastname, short_lastname, dates, date_creation, last_update) VALUES(:firstname, :firstname, :lastname, :lastname, :dates,NOW(), NOW() )";
                                    $stmt = $connPrevu->prepare($sql);
                                    $stmt->bindValue('firstname', $firstname);
                                    $stmt->bindValue("lastname", $lastname);
                                    $stmt->bindValue("dates", $dates);
                                    $stmt->execute();

                                    $sql = "SELECT id_author as id FROM author ORDER by id_author DESC LIMIT 1";
                                    $id_author = $connPrevu->fetchAssoc($sql);
                                    $id_author = $id_author['id'];

                                } //Si l'auteur existe, on récupère son id
                                else {
                                    $sql_check = "SELECT id_author as id FROM author WHERE firstname = :firstname AND lastname = :lastname AND dates = :dates LIMIT 1";
                                    $stmt = $connPrevu->prepare($sql_check);
                                    $stmt->bindValue("firstname", $firstname);
                                    $stmt->bindValue("lastname", $lastname);
                                    $stmt->bindValue("dates", $dates);
                                    $stmt->execute();
                                    $id_author = $stmt->fetch();
                                    $id_author = $id_author['id'];
                                }

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée juste avant l'insert de l'auteur = ".$now." secondes.</p>";

                                //Insert des notices (attention aux risques d'injection sql)

                                $sql_notice = "INSERT INTO prevu.book(id_book, title, author, publicationyear, isbn, date_creation, last_update)(SELECT :prevu, biblio.title, biblio.author, biblioitems.publicationyear, biblioitems.isbn, NOW(), NOW() FROM koha.biblio INNER JOIN koha.biblioitems ON biblio.biblionumber = biblioitems.biblionumber  WHERE biblio.biblionumber > :last LIMIT 1);";

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée juste après l'insert de l'auteur = ".$now." secondes.</p>";

                                break;

                            //Roubais
                            case 2:
                                //Insert des nouveaux auteurs

                                $sql_author = "SELECT EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"a\"]') as lastname, EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"b\"]') as firstname,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"f\"]') as dates FROM biblioitems WHERE biblionumber > :id LIMIT 1;";
                                $stmt = $conn->prepare($sql_author);
                                $stmt->bindValue("id", $last_id);
                                $stmt->execute();
                                $author = $stmt->fetch();

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée après sélection des auteurs = ".$now." secondes.</p>";

                                if(ISSET($author['firstname'])){
                                    $firstname = utf8_encode(addslashes($author['firstname']));
                                }
                                else{
                                    $firstname = '';
                                }

                                if(ISSET($author['lastname'])){
                                    $lastname = utf8_encode(addslashes($author['lastname']));
                                }
                                else{
                                    $lastname = '';
                                }

                                if(ISSET($author['dates'])){
                                    $dates = utf8_encode(addslashes($author['dates']));
                                }
                                else{
                                    $dates = '';
                                }

                                //dump($lastname);die;
                                //on ne devrait pas arriver à cette ligne puisque toutes les notices ont déjà été importées, seules les associations manquent

                                //on vérifie si l'auteur existe déjà dans Prévu
                                $sql_check = "SELECT COUNT(*) as nb FROM author WHERE short_firstname = :firstname AND short_lastname = :lastname AND dates = :dates ";
                                $stmt = $connPrevu->prepare($sql_check);
                                $stmt->bindValue("firstname", $firstname);
                                $stmt->bindValue("lastname", $lastname);
                                $stmt->bindValue("dates", $dates);
                                $stmt->execute();
                                $checkAuthor = $stmt->fetch();
                                $checkAuthor = $checkAuthor['nb'];

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée après vérification doublons  auteurs = ".$now." secondes.</p>";

                                //Si l'auteur n'existe pas déjà, on insère ses infos
                                if ($checkAuthor < 1) {

                                    $sql = "INSERT INTO author (firstname, short_firstname, lastname, short_lastname, dates, date_creation, last_update) VALUES(:firstname, :firstname, :lastname, :lastname, :dates,NOW(), NOW() )";
                                    $stmt = $connPrevu->prepare($sql);
                                    $stmt->bindValue('firstname', $firstname);
                                    $stmt->bindValue("lastname", $lastname);
                                    $stmt->bindValue("dates", $dates);
                                    $stmt->execute();

                                    $sql = "SELECT id_author as id FROM author ORDER by id_author DESC LIMIT 1";
                                    $id_author = $connPrevu->fetchAssoc($sql);
                                    $id_author = $id_author['id'];

                                } //Si l'auteur existe, on récupère son id
                                else {
                                    $sql_check = "SELECT id_author as id FROM author WHERE firstname = :firstname AND lastname = :lastname AND dates = :dates LIMIT 1";
                                    $stmt = $connPrevu->prepare($sql_check);
                                    $stmt->bindValue("firstname", $firstname);
                                    $stmt->bindValue("lastname", $lastname);
                                    $stmt->bindValue("dates", $dates);
                                    $stmt->execute();
                                    $id_author = $stmt->fetch();
                                    $id_author = $id_author['id'];
                                }

                                //insert des notices
                                $sql_notice = "INSERT INTO prevu.book(id_book, title, author, publicationyear, isbn, date_creation, last_update)(SELECT :prevu, biblio.title, biblio.author, biblioitems.publicationyear, biblioitems.isbn, NOW(), NOW() FROM prevu_rbx.biblio INNER JOIN prevu_rbx.biblioitems ON biblio.biblionumber = biblioitems.biblionumber  WHERE biblio.biblionumber > :last LIMIT 1);";

                                break;

                            //Saclay
                            case 3:
                                //Insert des nouveaux auteurs

                                $sql_author = "SELECT EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"a\"]') as lastname, EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"b\"]') as firstname,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"f\"]') as dates FROM biblioitems WHERE biblionumber > :id LIMIT 1;";
                                $stmt = $conn->prepare($sql_author);
                                $stmt->bindValue("id", $last_id);
                                $stmt->execute();
                                $author = $stmt->fetch();

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée après sélection des auteurs = ".$now." secondes.</p>";

                                //voir si on ne supprime pas le encode
                                if(ISSET($author['firstname'])){
                                    $firstname = utf8_encode(addslashes($author['firstname']));
                                }
                                else{
                                    $firstname = '';
                                }

                                if(ISSET($author['lastname'])){
                                    $lastname = utf8_encode(addslashes($author['lastname']));
                                }
                                else{
                                    $lastname = '';
                                }

                                if(ISSET($author['dates'])){
                                    $dates = utf8_encode(addslashes($author['dates']));
                                }
                                else{
                                    $dates = '';
                                }

                                //on vérifie si l'auteur existe déjà dans Prévu
                                $sql_check = "SELECT COUNT(*) as nb FROM author WHERE short_firstname = :firstname AND short_lastname = :lastname AND dates = :dates ";
                                $stmt = $connPrevu->prepare($sql_check);
                                $stmt->bindValue("firstname", $firstname);
                                $stmt->bindValue("lastname", $lastname);
                                $stmt->bindValue("dates", $dates);
                                $stmt->execute();
                                $checkAuthor = $stmt->fetch();
                                $checkAuthor = $checkAuthor['nb'];

                                $now = microtime(true) - $item_time;
                                echo "<p>Durée après vérification doublons  auteurs = ".$now." secondes.</p>";

                                //Si l'auteur n'existe pas déjà, on insère ses infos
                                if ($checkAuthor < 1) {

                                    $sql = "INSERT INTO author (firstname, short_firstname, lastname, short_lastname, dates, date_creation, last_update) VALUES(:firstname, :firstname, :lastname, :lastname, :dates,NOW(), NOW() )";
                                    $stmt = $connPrevu->prepare($sql);
                                    $stmt->bindValue('firstname', $firstname);
                                    $stmt->bindValue("lastname", $lastname);
                                    $stmt->bindValue("dates", $dates);
                                    $stmt->execute();

                                    $sql = "SELECT id_author as id FROM author ORDER by id_author DESC LIMIT 1";
                                    $id_author = $connPrevu->fetchAssoc($sql);
                                    $id_author = $id_author['id'];

                                } //Si l'auteur existe, on récupère son id
                                else {
                                    $sql_check = "SELECT id_author as id FROM author WHERE firstname = :firstname AND lastname = :lastname AND dates = :dates LIMIT 1";
                                    $stmt = $connPrevu->prepare($sql_check);
                                    $stmt->bindValue("firstname", $firstname);
                                    $stmt->bindValue("lastname", $lastname);
                                    $stmt->bindValue("dates", $dates);
                                    $stmt->execute();
                                    $id_author = $stmt->fetch();
                                    $id_author = $id_author['id'];
                                }

                                //insert des notices
                                $sql_notice = "INSERT INTO prevu.book(id_book, title, author, publicationyear, isbn, date_creation, last_update)(SELECT :prevu, biblio.title, biblio.author, biblioitems.publicationyear, biblioitems.isbn, NOW(), NOW() FROM prevu_saclay.biblio INNER JOIN prevu_saclay.biblioitems ON biblio.biblionumber = biblioitems.biblionumber  WHERE biblio.biblionumber > :last LIMIT 1);";

                                break;

                        }//end switch

                        //reste CDU, DEWEY : CDU : itemcallnumber d'items
                        $stmt = $conn->prepare($sql_notice);
                        $stmt->bindValue("prevu", $id_prevu);
                        $stmt->bindValue("last", $last_id);
                        $stmt->execute();

                        $now = microtime(true) - $item_time;
                        echo "<p>Durée après insert notice = ".$now." secondes.</p>";


                    } //enf if : si la notice n'existe pas, on vient d'ajouter son auteur, la notice et le lien de l'auteur avec la notice


                    //-----------------------------------------------------------
                    //Ajout de la clé de la notice
                    //-----------------------------------------------------------

                    //si la notice existait déjà
                    else{

                        //Il faut récupérer son id
                        $sql = "SELECT id_book as id FROM book as b INNER JOIN association as k ON k.prevu = b.id_book WHERE title = :title AND isbn = :isbn AND library != :library AND k.type = 'book' ";
                        $stmt = $connPrevu->prepare($sql);
                        $stmt->bindValue("title", $title);
                        $stmt->bindValue("isbn", $isbn);
                        $stmt->bindValue("library", $library);
                        $stmt->execute();
                        $id_prevu = $stmt->fetch();
                        $id_prevu = $id_prevu['id'];

                        //il faut récupérer son auteur

                        //quand il y a doublon, il n'y plus de id_prevu
                    }

//                    dump($exist);die();

                    $sql = "INSERT INTO prevu.association(prevu, koha, type, library, date_creation, last_update) VALUES( :id_prevu, :id_koha,:type, :library , NOW(), NOW() );";

                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue("id_prevu", $id_prevu);
                    $stmt->bindValue("id_koha", $id_koha);
                    $stmt->bindValue("type", 'book');
                    $stmt->bindValue("library", $library);
                    $stmt->execute();

                    $cmp = $i+1;

//                    dump($exist);die;

                    //si la notice n'existait pas déjà, on ajoute l'auteur
                    if($exist < 1){
                        //-----------------------------------------------------------
                        //Ajout du mains auteur : relation book / author dans book
                        //-----------------------------------------------------------
                        $sql = "UPDATE book SET id_author = :author WHERE id_book = :book";
                        $stmt = $connPrevu->prepare($sql);
                        $stmt->bindValue("author", $id_author);
                        $stmt->bindValue("book", $id_prevu);
                        $stmt->execute();
                    }

                    //-----------------------------------------------------------
                    //Ajout des auteurs secondaires : relation book / author many to many dans table intermédiaire - TODO
                    //-----------------------------------------------------------


                    //to do récupérer plusieurs auteurs = utiliser cette requête et enlever l'autre, ou alors la déplacer ailleurs
//                                $sql_author = "SELECT EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"a\"]') as lastname1, EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"b\"]') as firstname1,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"700\"]/subfield[@code=\"f\"]') as dates1, EXTRACTVALUE(marcxml,'//datafield[@tag=\"701\"]/subfield[@code=\"a\"]') as lastname2, EXTRACTVALUE(marcxml,'//datafield[@tag=\"701\"]/subfield[@code=\"b\"]') as firstname2,  EXTRACTVALUE(marcxml,'//datafield[@tag=\"701\"]/subfield[@code=\"f\"]') as dates2  FROM biblioitems WHERE biblionumber > :id LIMIT 1;";

                    $now = microtime(true) - $item_time;

                    echo "<p> Nombre de livres sauvegardés jusqu'à présent : " . $cmp . ". Durée de la sauvegarde = <strong>".$now."</strong> secondes.</p><hr>"; //A voir

                }//end if : si le biblio n'est pas déjà dans cette biblio - s'il l'est, c'est que l'import est déjà fait pour cette notice

            }//end else du début de l'import après vérification qu'il n'a pas déjà été fait

        }//endfor


            //créer les index
//            $sql ="CREATE INDEX `index_book` ON `book`(`publicationyear`, `short_title`, `isbn`)";
//            $sql2 ="CREATE INDEX `index_author` ON `author`(`dates`, `short_firstname`, `short_lastname`)";

//            $stmt = $connPrevu->exec($sql);

            $now = microtime(true) - $start_time;

            echo "<p>Durée complète de l'import = <strong>".$now."</strong> secondes.</p>";
            return $this->render('ImportBundle:Default:index.html.twig');

    }

    /**
     * @Route("/import/delete/books", name="delete_books")
     */
    public function deleteAllBooks()
    {

        $dbuser = $this->container->getParameter('database_user');
        $password = $this->container->getParameter('database_password');
        $host = $this->container->getParameter('database_host');

        $user = $this->getUser();

        if($user->hasRole('ROLE_SUPER_ADMIN')){
            $connectionParams = array(
                'dbname' => 'prevu',
                'user' => $dbuser,
                'password' => $password,
                'host' => $host,
                'driver' => 'pdo_mysql',
            );


            $config = new \Doctrine\DBAL\Configuration();
            $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

//            DELETE FROM `itemtype` WHERE 1
//            DELETE FROM `code` WHERE 1
    //                $sql_issue = DELETE FROM `issue` WHERE 1
    //            DELETE FROM `item` WHERE 1
    //                $sql_key = "DELETE FROM association WHERE 1;";
    //                $sql_book = "DELETE FROM `book` WHERE 1;";
//            DELETE FROM `author` WHERE 1

    //
    //                $sql = $sql_key.$sql_book;
    //                $stmt = $conn->prepare($sql);
    //                $stmt->execute();

            $this->addFlash('success', 'Suppression des données');

            return $this->redirectToRoute('import');
        }

        else{

            $this->addFlash('success', 'Vous ne pouvez pas supprimer les données');

            return $this->redirectToRoute('import');
        }


        return $this->render('ImportBundle:Default:index.html.twig');
    }

}

