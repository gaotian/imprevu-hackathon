<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/import", name="import")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $stats = $em->getRepository('AppBundle:Statistique')->findAll();

        return $this->render('ImportBundle:Default:index.html.twig' , array(
            'stats' => $stats
        ));
    }

    /**
     * @Route("/import/check", name="import_check")
     */
    public function importCheck()
    {
        $em = $this->getDoctrine()->getManager();

        $stats = $em->getRepository('AppBundle:Statistique')->findAll();
//        $top100Up8 = $em->getRepository('AppBundle:Issue')->top100BooksFor1library(1);
//        $top100Rbx = $em->getRepository('AppBundle:Issue')->top100BooksFor1library(2);
//        $top100Scl = $em->getRepository('AppBundle:Issue')->top100BooksFor1library(3);


        return $this->render('ImportBundle:Check:index.html.twig', array(
            'stats' => $stats,
//            'top100Up8' => $top100Up8,
//            'top100Rbx' => $top100Rbx,
//            'top100Scl' => $top100Scl
        ));
    }

}

// php bin/console doctrine:mapping:import --force ImportBundle yml