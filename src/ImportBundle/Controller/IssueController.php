<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class IssueController extends Controller
{
    public function insert($db_name, $base, $library){

        $user = $this->container->getParameter('database_user');
        $password = $this->container->getParameter('database_password');
        $host = $this->container->getParameter('database_host');

        $connectionParamsPrevu = array(
            'dbname' => 'prevu',
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $connectionParams = array(
            'dbname' => $db_name,
            'user' => $user,
            'password' => $password,
            'host' => $host,
            'driver' => 'pdo_mysql',
        );

        $config = new \Doctrine\DBAL\Configuration();
        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        $connPrevu = \Doctrine\DBAL\DriverManager::getConnection($connectionParamsPrevu, $config);

//        ajout des issues
        $sql = "INSERT INTO prevu.issue(`koha_borrower`, `koha_item`, `sex`, `issuedate`, `returndate`, `renewals`, `date_creation`,`last_update`, `id_library`)(SELECT i.borrowernumber, i.itemnumber, b.sex, i.issuedate, i.returndate, i.renewals, NOW(), NOW(),:library FROM ".$base.".old_issues as i LEFT JOIN ".$base.".borrowers as b ON i.borrowernumber = b.borrowernumber)";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("library", $library);
        $stmt->execute();

        //!!! Ajouter index issue.koha_item et à item.koha
        //update des notices livres pour les prêts (ne pas oublier d'ajouter l'index à issue.koha_item et à item.koha) // cette requête est fausse
        $sql = "UPDATE issue i INNER JOIN item t ON t.koha = i.koha_item SET i.id_item = t.id_item WHERE i.id_library = :library AND t.library = :library";
        $stmt = $connPrevu->prepare($sql);
        $stmt->bindValue("library", $library);
        $stmt->execute();

        //update des emprunteurs prévu pour les prêts (105.3466 sec )
        $sql = "UPDATE issue i INNER JOIN borrower b ON b.koha = i.koha_borrower SET i.id_borrower= b.id_borrower WHERE i.id_library = :library";
        $stmt = $connPrevu->prepare($sql);
        $stmt->bindValue("library", $library);
        $stmt->execute();

    }

    /**
     * @Route("/import/issues/lib={library}", name="import_issues")
     */
    public function importIssuesAction($library)
    {

        switch ($library) {
            //Base de données Koha de Paris 8
            case "up8":
                $dbname = $this->container->getParameter('database_name2');

                $library = 1;

                $this->insert($dbname, "koha", $library);


                break;

            //Base de données Koha de Roubaix
            case "rbx":
                $dbname = $this->container->getParameter('database_name3');

                $library = 2;

                $this->insert($dbname, "prevu_rbx", $library);

            //Base de données Koha de Saclay
            case "scl":
                $dbname = $this->container->getParameter('database_name4');

                $library = 3;

                $this->insert($dbname, "prevu_saclay", $library);

                break;
            default :
                return $this->render('ImportBundle:Book:index.html.twig');
                break;
        }
        return $this->render('ImportBundle:Default:index.html.twig');
    }

    /**
     * @Route("/import/issues/books/lib={library}", name="import_issues_books")
     */
    public function importIssuesBookAction($library)
    {

    }

    /**
     * @Route("/import/issues/books/lib={library}", name="import_issues_books")
     */
    public function importIssuesBorrowerAction($library)
    {

    }
}
