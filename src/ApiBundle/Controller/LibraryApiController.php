<?php
/**
 * Created by PhpStorm.
 * User: Ale PDERY
 * Date: 19/05/2017
 * Time: 14:04
 */


namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Book;
use AppBundle\Form\BookType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class LibraryApiController extends Controller
{

    /**
     * @Route("/api/biblio/annee/{id}/{mois}", name="api_biblio_mois")
     */
    public function emprunt_by_date($id , $mois)
    {
        $em = $this->getDoctrine()->getManager();

        if ($mois == "00") {
            $query = $em->createQuery("SELECT UNIX_TIMESTAMP(i.returndate) as label, COUNT(i.returndate) as value FROM AppBundle:Issue i WHERE i.idlibrary = :id GROUP BY i.returndate ORDER BY i.returndate ASC");
        }
        else {
            $query = $em->createQuery("SELECT UNIX_TIMESTAMP(i.returndate) as label, COUNT(i.returndate) as value FROM AppBundle:Issue i WHERE i.idlibrary = :id and MONTH (i.returndate) = $mois GROUP BY i.returndate ORDER BY i.returndate ASC");
        }

        $query->setParameter('id', $id);
        $returns = $query->getResult();

        foreach ($returns as $key => $item){
            $returns[$key]['label'] = (int)$returns[$key]['label'];
            $returns[$key]['value'] = (int)$returns[$key]['value'];
        }

        return new JsonResponse($returns);
    }


    /**
     * @Route("/api/biblio/genres/{id}/", name="api_biblio_genre_by_biblio")
     */
    function get_genre_biblio_by_id($id) {

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(DISTINCT(b.idborrower)) as value, b.sex as label  FROM AppBundle:Issue b WHERE b.idlibrary = :id GROUP BY label");
        $query->setParameter('id', $id);
        $nbEmpruntsBySex = $query->getResult();

        foreach ($nbEmpruntsBySex as $key => $item){
            $nbEmpruntsBySex[$key]['value'] = (int)$nbEmpruntsBySex[$key]['value'];
        }

        return new JsonResponse($nbEmpruntsBySex);
    }

}



