<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Book;
use AppBundle\Form\BookType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class BnController extends Controller
{
    /**
     * @Route("/api/bn/hours", name="api_bn_hours")
     */
    public function hoursAction()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value, s.day_short as day, s.hour as hour FROM AppBundle:StatsBNLogs s GROUP BY s.day, s.hour ORDER BY value DESC");
        $days = $query->getResult();

//        return new Response($days[0]['day'].",".$days[0]['hour'].",".$days[0]['nb']);
        return new JsonResponse($days);

        //Import MySQL

        //UPDATE `stats_bn_logs` SET `day_short`= 1 WHERE day = "lundi";
        //UPDATE `stats_bn_logs` SET `day_short`= 2 WHERE day = "mardi";
        //UPDATE `stats_bn_logs` SET `day_short`= 3 WHERE day = "mercredi";
        //UPDATE `stats_bn_logs` SET `day_short`= 4 WHERE day = "jeudi";
        //UPDATE `stats_bn_logs` SET `day_short`= 5 WHERE day = "vendredi";
        //UPDATE `stats_bn_logs` SET `day_short`= 6 WHERE day = "samedi";
        //UPDATE `stats_bn_logs` SET `day_short`= 7 WHERE day = "dimanche";
    }

    /**
     * @Route("/api/bn/hours/csv", name="api_bn_hours_csv")
     */
    public function hourscsvAction()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value, s.day_short as day, s.hour as hour FROM AppBundle:StatsBNLogs s GROUP BY s.day, s.hour ORDER BY value DESC");
        $days = $query->getResult();

        $msg = "day,hour,value<br/>";

        foreach ($days as $item) {
            if ($item['hour'] == 0) {
                $item['hour'] = 24;
            }
            $msg .= $item['day'] . "," . $item['hour'] . "," . $item['value'] . "<br/>";
        }

        $response = new Response($msg);

        $response->setContent($msg);

        return $response;
    }

    /**
     * @Route("/api/bn/hours/json/{mois}", name="api_bn_hours_json")
     */
    public function hourscsvActionparannee($mois)
    {
        $em = $this->getDoctrine()->getManager();

        //le annee 00 équivaut à ALL
        if ($mois == "00") {
            $mois = "";
        }

        $query = $em->createQuery("SELECT s.day_short as day, s.hour as hour, COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%2017-" . $mois . "%' GROUP BY s.hour, s.day_short ORDER BY day, hour DESC ");
        $days = $query->getResult();

        foreach ($days as $key => $item) {

            $days[$key]['value'] = (int)$days[$key]['value'];
            if ($item['hour'] == 0) {
                $days[$key]['hour'] = 24;
            }
        }

        return new JsonResponse($days);
    }


    /**
     * @Route("/api/bn/csp/genre/{csp}", name="api_bn_csp_genre")
     */
    public function cspGenre($csp)
    {
        $em = $this->getDoctrine()->getManager();


        $query = $em->createQuery("SELECT  s.gender as label, COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.csp = '" . $csp . "' GROUP BY s.gender ORDER BY value ");
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/bn/csp/prets/{csp}", name="api_bn_csp_pret")
     */
    public function cspPret($csp)
    {
        $em = $this->getDoctrine()->getManager();


        $query = $em->createQuery("SELECT COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.csp = '" . $csp . "'");
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/bn/csp/lecteurs/{csp}", name="api_bn_csp_lecteurs")
     */
    public function cspLecteurs($csp)
    {
        $em = $this->getDoctrine()->getManager();


        $query = $em->createQuery("SELECT COUNT(DISTINCT s.cb) as value FROM AppBundle:StatsBNLogs s WHERE s.csp = '" . $csp . "'");
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/bn/csp/age_moyen/{csp}", name="api_bn_csp_age_moyen")
     */
    public function cspAgeMoyen($csp)
    {
        $em = $this->getDoctrine()->getManager();


        $annee = date('Y');

        $query = $em->createQuery("SELECT AVG(" . $annee . " - s.birthday) as value FROM AppBundle:StatsBNLogs s WHERE s.csp = '" . $csp . "'");
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/bn/csp/top_books/{csp}", name="api_bn_csp_top_books")
     */
    public function cspTopBooks($csp)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.title as titre, s.author as auteur, s.ean, count(s) as value FROM AppBundle:StatsBNLogs s WHERE s.csp = '" . $csp . "' GROUP BY titre ORDER BY value DESC");
        $query->setMaxResults(10);
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/bn/annee/{annee}/{mois}", name="api_bn_annee")
     */
    public function emprunt_by_month($annee, $mois)
    {
        $em = $this->getDoctrine()->getManager();

        if ($mois == "00") {
            $mois = "";
        }

        $query = $em->createQuery("SELECT UNIX_TIMESTAMP(s.date) as label , COUNT(s) as value, s.day_short as day FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%".$annee."-" . $mois . "%' GROUP by s.date ORDER BY s.date");
        $ret = $query->getResult();

        foreach ($ret as $key => $item) {
            $ret[$key]['value'] = (int)$ret[$key]['value'];
            $ret[$key]['label'] = (int)$ret[$key]['label'];
            if ($ret[$key]['day'] == 1) {
                $ret[$key]['day'] = "Dimanche";
            } else if ($ret[$key]['day'] == 2) {
                $ret[$key]['day'] = "Lundi";
            } else if ($ret[$key]['day'] == 3) {
                $ret[$key]['day'] = "Mardi";
            } else if ($ret[$key]['day'] == 4) {
                $ret[$key]['day'] = "Mercredi";
            } else if ($ret[$key]['day'] == 5) {
                $ret[$key]['day'] = "Jeudi";
            } else if ($ret[$key]['day'] == 6) {
                $ret[$key]['day'] = "Vendredi";
            } else {
                $ret[$key]['day'] = "Samedi";
            }
        }

        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/bn/csp/annee/{csp}/{annee}", name="api_bn_csp_annee")
     */
    public function cspJour($csp, $annee)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT UNIX_TIMESTAMP(s.date) as label , COUNT(s) as value, s.day_short as day FROM AppBundle:StatsBNLogs s WHERE s.csp = '" . $csp . "' and YEAR(s.date) = '" . $annee . "'  GROUP by s.date ORDER BY s.date");
        $ret = $query->getResult();

        foreach ($ret as $key => $item) {
            $ret[$key]['value'] = (int)$ret[$key]['value'];
            $ret[$key]['label'] = (int)$ret[$key]['label'];
            if ($ret[$key]['day'] == 1) {
                $ret[$key]['day'] = "Dimanche";
            } else if ($ret[$key]['day'] == 2) {
                $ret[$key]['day'] = "Lundi";
            } else if ($ret[$key]['day'] == 3) {
                $ret[$key]['day'] = "Mardi";
            } else if ($ret[$key]['day'] == 4) {
                $ret[$key]['day'] = "Mercredi";
            } else if ($ret[$key]['day'] == 5) {
                $ret[$key]['day'] = "Jeudi";
            } else if ($ret[$key]['day'] == 6) {
                $ret[$key]['day'] = "Vendredi";
            } else {
                $ret[$key]['day'] = "Samedi";
            }
        }


        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/bn/cartes/carteIleDeFrance/{mois}", name="api_bn_carte_ile_de_france")
     */
    public function popDeptIleDeFrance($mois)
    {
        $em = $this->getDoctrine()->getManager();

        if ($mois == "00") {
            $mois = "";
        }


        $query = $em->createQuery("SELECT s.adresse AS Dept, s.zipcode, COUNT('*') AS Pop_par_Dept FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%2017-" . $mois . "%' GROUP BY s.adresse HAVING substring(s.zipcode,1,2) != 75 ORDER BY s.zipcode");
        $ret = $query->getResult();

        foreach ($ret as $key => $item) {
            $ret[$key]['Pop_par_Dept'] = (int)$ret[$key]['Pop_par_Dept'];
            $ret[$key]['zipcode'] = (int)$ret[$key]['zipcode'];
        }

        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/bn/cartes/carteParis/{mois}", name="api_bn_carte_paris")
     */
    public function popDeptParis($mois)
    {
        $em = $this->getDoctrine()->getManager();

        if ($mois == "00") {
            $mois = "";
        }

        $query = $em->createQuery("SELECT s.adresse AS Dept, s.zipcode, COUNT('*') AS Pop_par_Dept FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%2017-" . $mois . "%' GROUP BY s.adresse HAVING substring(s.zipcode,1,3) = 751 ORDER BY s.zipcode");
        $ret = $query->getResult();

        foreach ($ret as $key => $item) {
            $ret[$key]['Pop_par_Dept'] = (int)$ret[$key]['Pop_par_Dept'];
            $ret[$key]['zipcode'] = (int)$ret[$key]['zipcode'];
        }

        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/bn/csp/classification", name="api_bn_csp_classification")
     */
    public function cspClassification()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT s.classification as name , COUNT(s) as value  FROM AppBundle:StatsBNLogs s WHERE s.classification != '' GROUP by name ORDER BY value DESC")->setMaxResults(10);
        $ret = $query->getResult();

        foreach ($ret as $key => $item) {
            $ret[$key]['value'] = (int)$ret[$key]['value'];
        }

        return new JsonResponse($ret);
    }

    //Répartition des genres (pour la treemap)

    /**
     * @Route("/api/bn/classification/{mois}", name="api_bn_classification")
     */
    public function classification($mois)
    {
        $em = $this->getDoctrine()->getManager();

        if ($mois == "00") {
            $mois = "";
        }


        $query = $em->createQuery("SELECT COUNT( s.title) as value ,s.classification as key FROM AppBundle:StatsBNLogs s WHERE s.classification != '' AND s.date LIKE '%2017-" . $mois . "%' GROUP by key ORDER BY value DESC")->setMaxResults(10);
        $classification = $query->getResult();

        foreach ($classification as $key => $item) {
            $classification[$key]['value'] = (int)$classification[$key]['value'];
        }

        return new JsonResponse($classification);
    }



    //Nombre de livres empruntés  pour un mois ou pour tous avec all

    /**
     * @Route("/api/bn/top/books/{mois}", name="api_bn_topbooks")
     */
    public function getTopBooks($mois)
    {
        $em = $this->getDoctrine()->getManager();

        if ($mois == "00") {
            $mois = "";
        }


        $query = $em->createQuery("SELECT COUNT(s.ean) as nb, s.title as title, s.ean as ean, s.author as author, s.image as image FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%2017-" . $mois . "%' GROUP BY s.ean ORDER BY nb DESC")->setMaxResults(8);
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }

    //Nombre de livres empruntés pour une période

    /**
     * @Route("/api/bn/top/books/period/begin={dateBegin}/end={dateEnding}", name="api_bn_topbooks_period")
     */
    public function getTopBooksForPeriod($dateBegin, $dateEnding)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s.ean) as nb, s.title as title, s.ean as ean, s.author as author FROM AppBundle:StatsBNLogs s WHERE s.date BETWEEN :dateBegin AND :dateEnding GROUP BY s.ean ORDER BY nb DESC")->setMaxResults(8);
        $query->setParameter('dateBegin', $dateBegin);
        $query->setParameter('dateEnding', $dateEnding);
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/bn/top/authors/{mois}", name="api_bn_topauthors")
     */
    public function getTopAuthors($mois)
    {
        $em = $this->getDoctrine()->getManager();

        if ($mois == "00") {
            $mois = "";
        }


        $query = $em->createQuery("SELECT COUNT(s.author)as nb, s.author as author FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%2017-" . $mois . "%' GROUP BY s.author ORDER BY nb DESC")->setMaxResults(8);
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/bn/AllCsp/{mois}", name="api_bn_donut_csp")
     */
    public function getAllCsp($mois)
    {
        $em = $this->getDoctrine()->getManager();

        if ($mois == "00") {
            $mois = "";
        }


        $query = $em->createQuery("SELECT s.csp as label, COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%2017-".$mois."%' AND s.csp != '' GROUP BY s.csp ORDER BY value DESC");
        $ret = $query->getResult();


        foreach ($ret as $key => $item) {
            $ret[$key]['value'] = (int)$ret[$key]['value'];
        }

        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/bn/repartiongenre", name="api_bn_repartitiongenre")
     */

    public function getRepartitiongenre()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value , s.gender as label FROM AppBundle:StatsBNLogs s WHERE s.gender != '' GROUP BY s.gender ORDER BY value");
        $repartitionhommefemme = $query->getResult();

        return new JsonResponse($repartitionhommefemme);
    }

    /**
    * @Route("/api/bn/AllCsp/emprunteurs/{mois}", name="api_bn_donut_csp_emprunteurs")
    */

    public function getAllCspEmprunteurs($mois)
    {
            $em = $this->getDoctrine()->getManager();

            if ($mois == "00") {
                $mois = "";
            }

            $query = $em->createQuery("SELECT s.csp as label, COUNT(DISTINCT s.cb) as value FROM AppBundle:StatsBNLogs s WHERE s.date LIKE '%2017-" . $mois . "%' AND s.csp != '' GROUP BY s.csp ORDER BY value DESC");

            $ret = $query->getResult();


            foreach ($ret as $key => $item) {
                $ret[$key]['value'] = (int)$ret[$key]['value'];
            }


            return new JsonResponse($ret);

    }


    /**
     * @Route("/api/bn/repartiongenreauteur/{auteur}", name="api_bn_repartitiongenreauteur")
     */

    public function getRepartitiongenreparauteur($auteur)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value , s.gender as label FROM AppBundle:StatsBNLogs s WHERE s.author LIKE '$auteur%' GROUP BY s.gender ORDER BY value");
        $repartitionhommefemme = $query->getResult();

        return new JsonResponse($repartitionhommefemme);

    }

    /**
     * @Route("/api/bn/classificationauteur/{auteur}", name="api_bn_classificationauteur")
     */

    public function getClassificationAuteur($auteur)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value , s.classification as key FROM AppBundle:StatsBNLogs s WHERE s.author LIKE '$auteur%' GROUP BY s.classification ORDER BY value");
        $classificationbyauteur = $query->getResult();

        return new JsonResponse($classificationbyauteur);

    }

    /**
     * @Route("/api/bn/livreauteur/{auteur}", name="api_bn_livreauteur")
     */

    public function getLivreAuteur($auteur)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value , s.title as label FROM AppBundle:StatsBNLogs s WHERE s.author LIKE '$auteur%' GROUP BY s.title ORDER BY value DESC ")->setMaxResults(10);;
        $classificationbyauteur = $query->getResult();

        return new JsonResponse($classificationbyauteur);

    }

    /**
     * @Route("/api/book/{ean}/csp", name="api_book_csp")
     */
    public function getCspBookByEan($ean)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT  s.csp as label, COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.ean = '" . $ean . "' AND s.csp != '' GROUP BY s.csp ORDER BY value DESC");
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }

    /**
     * @Route("api/bn/statsGrosEmprunteurs", name="api_paris_bn_stats_gros_emprunteurs")
     */
    public function StatsGrosEmprunteurs()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.cb as emprunteur, COUNT(s) as nb_emprunts FROM AppBundle:StatsBNLogs s GROUP BY s.cb HAVING nb_emprunts = 11 or nb_emprunts = 12 ORDER BY emprunteur");
        $ret = $query->getResult();

        foreach ($ret as $key => $item) {
            $ret[$key]['nb_emprunts'] = (int)$ret[$key]['nb_emprunts'];
        }
        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/book/nb", name="api_nb_book")
     */
    public function getNbBooks() {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(DISTINCT s.title) as value FROM AppBundle:StatsBNLogs s");
        $ret = $query->getResult();


        $ret[0]['value'] = (int)$ret[0]['value'];

        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/csp/emprunts/pourcent/{ean}", name="api_csp_emprunt_pourcent")
     */
    public function getCspPourcent($ean)
    {
        $em = $this->getDoctrine()->getManager();

        //le total d'emprunts pour un livre
        $query = $em->createQuery("SELECT COUNT(s.id) as nb FROM AppBundle:StatsBNLogs s WHERE s.ean = :ean");
        $query->setParameter('ean', $ean);
        $totalOneBook = $query->getSingleResult();

        //le total de csp pour chaque csp d'un livre
        $query = $em->createQuery("SELECT COUNT(s.id) as nb, s.csp as nom FROM AppBundle:StatsBNLogs s WHERE s.ean = :ean GROUP BY s.csp ORDER BY nb DESC");
        $query->setParameter('ean', $ean);
        $query->setMaxResults(5);
        $oneBook = $query->getResult();

        //liste des 5 premiers csp d'un livre
        $listeCsp = [];
        foreach ($oneBook as $item) {
            array_push($listeCsp, $item['nom']);
        }

        //le total d'emprunts
        $query = $em->createQuery("SELECT COUNT(s.id) as nb FROM AppBundle:StatsBNLogs s");
        $totalAllCsp = $query->getSingleResult();

        //le total de csp pour chaque csp
        $query = $em->createQuery("SELECT COUNT(s.id) as nb, s.csp as nom FROM AppBundle:StatsBNLogs s WHERE s.csp IN (:listeCsp) GROUP BY s.csp ORDER BY nb DESC");
        $query->setParameter('listeCsp', $listeCsp);
        $allCsp = $query->getResult();

        $cspArray = array();
        for ($i = 0; $i < count($oneBook); $i++) {

            if (count($oneBook) == count($allCsp)) {

                $requete = $oneBook[$i]['nom'];
                $requete2 = $oneBook[$i]['nb'];
                $requete3 = $allCsp[$i]['nb'];


                $key = $i;

                $cspArray[$key]['label'] = $requete;
                $cspArray[$key]['valueBook'] = round((intval($requete2) / intval($totalOneBook['nb'])) * 100, 1, PHP_ROUND_HALF_UP);
                $cspArray[$key]['valueAll'] = round((intval($requete3) / intval($totalAllCsp['nb'])) * 100, 1, PHP_ROUND_HALF_UP);

            }
        }

        return new JsonResponse($cspArray);
    }


    /**
     * @Route("/api/bn/repartionGenreGrosEmprunteurs", name="api_bn_repartition_genre_gros_emprunteurs")
     */

    public function getRepartitiongenreGrosEmprunteurs()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.cb, COUNT(s) FROM AppBundle:StatsBNLogs s GROUP BY s.cb HAVING COUNT(s.cb) = 11 or COUNT(s.cb) = 12");
        $liste_gros_emrunteurs1 = $query->getResult();

        $query = $em->createQuery("SELECT s.gender as label, COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.cb IN (:liste) GROUP BY s.gender");
        $query->setParameter('liste',$liste_gros_emrunteurs1);
        $ratio_genre_gros_emprunteurs = $query->getResult();

        return new JsonResponse($ratio_genre_gros_emprunteurs);
    }

    /**
     * @Route("/api/bn/repartionCspGrosEmprunteurs", name="api_bn_repartition_csp_gros_emprunteurs")
     */

    public function getRepartitionCspGrosEmprunteurs()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.cb, COUNT(s) FROM AppBundle:StatsBNLogs s GROUP BY s.cb HAVING COUNT(s.cb) = 11 or COUNT(s.cb) = 12");
        $liste_gros_emrunteurs1 = $query->getResult();

        $query = $em->createQuery("SELECT COUNT(s.cb) as value, s.csp as label FROM AppBundle:StatsBNLogs s WHERE s.csp != '' AND s.cb IN (:liste) GROUP BY s.csp");
        $query->setParameter('liste', $liste_gros_emrunteurs1);
        $ratio_csp_gros_emprunteurs = $query->getResult();

        return new JsonResponse($ratio_csp_gros_emprunteurs);
    }

    /**
     * @Route("/api/bn/emprunteur1fevrier", name="api_bn_emprunteur1fevrier")
     */
    public function emprunteur_1_fevrier(){
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(DISTINCT s.cb) as value FROM AppBundle:StatsBNLogs s WHERE s.date = '2017-02-01'");
        $ret = $query->getResult();

        return new JsonResponse($ret);
    }


    /**
     * @Route("/api/bn/1fevrier", name="api_bn_1fevrier")
     */
    public function emprunt_1_fevrier()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT UNIX_TIMESTAMP(s.date) as label , COUNT(s) as value, s.day_short as day FROM AppBundle:StatsBNLogs s WHERE s.date = '2017-02-01'");
        $ret = $query->getResult();

        foreach ($ret as $key => $item){
            $ret[$key]['value'] = (int)$ret[$key]['value'];
            $ret[$key]['label'] = (int)$ret[$key]['label'];
            if($ret[$key]['day'] == 1){
                $ret[$key]['day'] = "Dimanche";
            }else if($ret[$key]['day'] == 2){
                $ret[$key]['day'] = "Lundi";
            }else if($ret[$key]['day'] == 3){
                $ret[$key]['day'] = "Mardi";
            }else if($ret[$key]['day'] == 4){
                $ret[$key]['day'] = "Mercredi";
            }else if($ret[$key]['day'] == 5){
                $ret[$key]['day'] = "Jeudi";
            }else if($ret[$key]['day'] == 6){
                $ret[$key]['day'] = "Vendredi";
            }else{
                $ret[$key]['day'] = "Samedi";
            }
        }

        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/bn/1fevriermoyenne", name="api_bn_1fevrier_moyenne")
     */
    public function moyenne_emprunt_1_fevrier()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(DISTINCT s.cb) as cbcount , COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.date = '2017-02-01'");
        $ret = $query->getResult();

        foreach ($ret as $key => $item){
            $ret[$key]['cbcount'] = (int)$ret[$key]['cbcount'];
            $ret[$key]['value'] = (int)$ret[$key]['value'];
        }

        return new JsonResponse($ret);
    }



    /**
     * @Route("/api/bn/1fevriersexe", name="api_bn_1fevrier_by_sexe")
     */
    public function emprunt_by_sexe_1_fevrier()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value, s.gender as label FROM AppBundle:StatsBNLogs s WHERE s.date = '2017-02-01' GROUP BY s.gender");
        $genres = $query->getResult();

        return new JsonResponse($genres);
    }

    /**
     * @Route("/api/bn/1fevriercsp", name="api_bn_1fevrier_by_csp")
     */
    public function emprunt_by_csp_1_fevrier()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value, s.csp as label FROM AppBundle:StatsBNLogs s WHERE s.date = '2017-02-01' GROUP BY s.csp ORDER BY value DESC");
        $ret = $query->getResult();


        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/bn/1fevrierhour", name="api_bn_1fevrier_by_hour")
     */
    public function emprunt_by_hour_1_fevrier()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.day_short as day, s.hour as hour, COUNT(s) as value FROM AppBundle:StatsBNLogs s WHERE s.date = '2017-02-01' GROUP BY s.hour, s.day_short ORDER BY day, hour DESC ");
        $days = $query->getResult();

        foreach ($days as $key => $item){

            $days[$key]['value'] = (int)$days[$key]['value'];
            if ($item['hour'] == 0 ) {
                $days[$key]['hour'] = 24;
            }
        }

        return new JsonResponse($days);
    }

    /**
     * @Route("/api/bn/1fevrieragemoyen", name="api_bn_1fevrier_age_moyen")
     */

    public function agemoyen1fev(){

        $em = $this->getDoctrine()->getManager();

        $temp = $em->createQuery("SELECT (YEAR(CURRENT_DATE()) - s.birthday) as value  FROM AppBundle:StatsBNLogs s WHERE s.birthday != '' AND s.date = '2017-02-01' GROUP BY s.cb");
        $temp1 = $temp->getResult();

        $result=0;
        $nbE = 0;
        foreach ($temp1 as $key => $item){
            $nbE++;
            $result += (int)$temp1[$key]['value'];
        }

        $moyennedage["value"] = $result/$nbE;

        return new JsonResponse($moyennedage);
    }

    /**
     * @Route("/api/bn/1fevrierempruntmoyen", name="api_bn_1fevrier_nombre_moyen_emprunt")
     */

    public function nombreempruntmoyen1fev(){

        $em = $this->getDoctrine()->getManager();

        // Moyenne d'emprunts par emprunteurs
        $query = $em->createQuery("SELECT COUNT(DISTINCT s.cb) FROM AppBundle:StatsBNLogs s WHERE s.date = '2017-02-01' ");
        $nbemprunteurs = $query->getSingleResult();

        $query = $em->createQuery("SELECT COUNT(s)  FROM AppBundle:StatsBNLogs s WHERE s.date = '2017-02-01'");
        $nbemprunts = $query->getSingleResult();


        $moyenneempruntbyemprunteur["value"] = (int)$nbemprunts[1]/(int)$nbemprunteurs[1];

        return new JsonResponse($moyenneempruntbyemprunteur);

    }

    /**
     * @Route("/api/bn/search/book/{title}", name="api_bn_search_title")
     */
    public function searchBook($title){

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.title as title, s.ean as ean, s.author as author FROM AppBundle:StatsBNLogs s WHERE s.title LIKE :book GROUP BY s.ean");
        $query->setParameter("book", "%".$title."%");
        $query->setMaxResults(30);
        $auteurs = $query->getResult();

        return new JsonResponse($auteurs);

    }

    /**
     * @Route("/api/bn/search/author/{author}", name="api_bn_search_author")
     */
    public function searchAuthor($author){

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.author as author FROM AppBundle:StatsBNLogs s WHERE s.author LIKE :auteur GROUP BY s.author");
        $query->setParameter("auteur", "%".$author."%");
        $query->setMaxResults(30);
        $auteurs = $query->getResult();

        return new JsonResponse($auteurs);

    }

    /**
     * @Route("/api/bn/search/authors", name="api_bn_search_authors")
     */
    public function searchAuthors(){

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT s.author as author FROM AppBundle:StatsBNLogs s GROUP BY s.author");
        $auteurs = $query->getResult();

        return new JsonResponse($auteurs);

    }


}


