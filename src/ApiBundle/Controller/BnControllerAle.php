<?php
/**
 * Created by PhpStorm.
 * User: Ale PDERY
 * Date: 16/05/2017
 * Time: 12:11
 */


namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Book;
use AppBundle\Form\BookType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class BnControllerAle extends Controller
{


    /**
     * @Route("/api/bn/classification", name="api_bn_all_classification")
     */
    public function allClassification()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value ,s.classification as key FROM AppBundle:StatsBNLogs s WHERE s.classification != '' GROUP by key ORDER BY value DESC")->setMaxResults(50);
        $classification = $query->getResult();

        foreach ($classification as $key => $item) {
            $classification[$key]['value'] = (int)$classification[$key]['value'];
        }

        return new JsonResponse($classification);
    }

    /**
     * @Route("/api/bn/classificationbook/{genre}", name="api_bn_book_classification")
     */
    public function book_by_classification($genre)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s.title) as nb , s.author as author, s.title as title  FROM AppBundle:StatsBNLogs s WHERE s.classification = '$genre' GROUP by title ORDER BY nb DESC")->setMaxResults(10);
      //  $query->setParameter('genre', $genre);
        $books = $query->getResult();

        foreach ($books as $key => $item) {
            $books[$key]['nb'] = (int)$books[$key]['nb'];
        }

        return new JsonResponse($books);
    }

    /**
     * @Route("/api/bn/classification/auteur/{genre}", name="api_bn_auteur_classification")
     */
    public function auteur_by_classification($genre)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s.author) as nb ,s.author as author FROM AppBundle:StatsBNLogs s WHERE s.classification = '$genre' GROUP by author ORDER BY nb DESC")->setMaxResults(10);
      //  $query->setParameter('genre', $genre);
        $books = $query->getResult();

        foreach ($books as $key => $item) {
            $books[$key]['nb'] = (int)$books[$key]['nb'];
        }

        return new JsonResponse($books);
    }

}

?>