<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170705190242 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE book ADD marcxml MEDIUMTEXT DEFAULT NULL, CHANGE work_title work_title MEDIUMTEXT DEFAULT NULL, CHANGE work_author work_author MEDIUMTEXT DEFAULT NULL');
        $this->addSql('DROP INDEX koha ON borrower');
        $this->addSql('DROP INDEX koha_borrower ON issue');
        $this->addSql('DROP INDEX koha_item ON issue');
        $this->addSql('DROP INDEX koha ON item');
        $this->addSql('ALTER TABLE thesaurus CHANGE description description VARCHAR(65535) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE book DROP marcxml, CHANGE work_title work_title MEDIUMTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE work_author work_author MEDIUMTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('CREATE INDEX koha ON borrower (koha)');
        $this->addSql('CREATE INDEX koha_borrower ON issue (koha_borrower)');
        $this->addSql('CREATE INDEX koha_item ON issue (koha_item)');
        $this->addSql('CREATE INDEX koha ON item (koha)');
        $this->addSql('ALTER TABLE thesaurus CHANGE description description MEDIUMTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
