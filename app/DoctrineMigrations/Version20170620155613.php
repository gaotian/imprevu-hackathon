<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170620155613 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE import_author');
        $this->addSql('ALTER TABLE book CHANGE work_title work_title MEDIUMTEXT DEFAULT NULL, CHANGE work_author work_author MEDIUMTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233E943B391C');
        $this->addSql('ALTER TABLE issue ADD borrower_type VARCHAR(45) DEFAULT NULL');
        $this->addSql('DROP INDEX id_item ON issue');
        $this->addSql('CREATE INDEX IDX_12AD233E943B391C ON issue (id_item)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E943B391C FOREIGN KEY (id_item) REFERENCES item (id_item)');
        $this->addSql('DROP INDEX id_item ON item');
        $this->addSql('DROP INDEX koha ON item');
        $this->addSql('ALTER TABLE thesaurus CHANGE description description VARCHAR(65535) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE import_author (id INT UNSIGNED AUTO_INCREMENT NOT NULL, firstname VARCHAR(500) NOT NULL COLLATE latin1_swedish_ci, lastname VARCHAR(500) NOT NULL COLLATE latin1_swedish_ci, dates VARCHAR(45) DEFAULT NULL COLLATE latin1_swedish_ci, id_book INT DEFAULT NULL, library INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book CHANGE work_title work_title MEDIUMTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE work_author work_author MEDIUMTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233E943B391C');
        $this->addSql('ALTER TABLE issue DROP borrower_type');
        $this->addSql('DROP INDEX idx_12ad233e943b391c ON issue');
        $this->addSql('CREATE INDEX id_item ON issue (id_item)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E943B391C FOREIGN KEY (id_item) REFERENCES item (id_item)');
        $this->addSql('CREATE INDEX id_item ON item (id_item)');
        $this->addSql('CREATE INDEX koha ON item (koha)');
        $this->addSql('ALTER TABLE thesaurus CHANGE description description MEDIUMTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
