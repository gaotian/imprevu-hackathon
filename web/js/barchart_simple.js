/**
 * Created by melvi on 11/05/2017.
 */
function barchart_simple(container, data){

    document.getElementById(container).innerHTML = "";

    var m = [30, 50, 10, 300],
        w = 760 - m[1] - m[3],
        h = 330 - m[0] - m[2];

    var format = d3.format(",.0f");

    var x = d3.scale.linear().range([0, w]),
        y = d3.scale.ordinal().rangeRoundBands([0, h], .1);

    var xAxis = d3.svg.axis().scale(x).orient("top").tickSize(-h),
        yAxis = d3.svg.axis().scale(y).orient("left").tickSize(0);

    var svg = d3.select("#"+container).append("svg")
        .attr("id", "svg_classification")
        .attr("width", w + m[1] + m[3])
        .attr("height", h + m[0] + m[2])
        .append("g")
        .attr("transform", "translate(" + m[3] + "," + m[0] + ")");


    x.domain([0, d3.max(data, function(d) { return d.value; })]);
    y.domain(data.map(function(d) { return d.name; }));

    var bar = svg.selectAll("g.bar")
        .data(data)
        .enter().append("g")
        .attr("class", "bar")
        .attr("transform", function(d) { return "translate(0," + y(d.name) + ")"; });

    bar.append("rect")
        .attr("width", function(d) { return x(d.value); })
        .attr("height", y.rangeBand());

    bar.append("text")
        .attr("class", "value")
        .attr("x", function(d) { return x(d.value); })
        .attr("y", y.rangeBand() / 2)
        .attr("dx", 25)
        .attr("dy", ".35em")
        .attr("text-anchor", "end")
        .text(function(d) { return format(d.value); });

    svg.append("g")
        .attr("class", "x axis")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

}

function barchart_vertical(container,data){

    nv.addGraph(function() {
        var chart = nv.models.discreteBarChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .staggerLabels(true)
            .showValues(true)

        d3.select(container)
            .datum(exampleData())
            .transition().duration(500)
            .call(chart)
        ;

        nv.utils.windowResize(chart.update);

        return chart;
    });

}

function exampleData() {
    return  [
        {
            key: "Cumulative Return",
            values: [
                {
                    "label" : "A Label" ,
                    "value" : -29.765957771107
                } ,
                {
                    "label" : "B Label" ,
                    "value" : 0
                } ,
                {
                    "label" : "C Label" ,
                    "value" : 32.807804682612
                } ,
                {
                    "label" : "D Label" ,
                    "value" : 196.45946739256
                } ,
                {
                    "label" : "E Label" ,
                    "value" : 0.19434030906893
                } ,
                {
                    "label" : "F Label" ,
                    "value" : -98.079782601442
                } ,
                {
                    "label" : "G Label" ,
                    "value" : -13.925743130903
                } ,
                {
                    "label" : "H Label" ,
                    "value" : -5.1387322875705
                }
            ]
        }
    ]

}