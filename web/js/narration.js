
function hideAllFragments(){
    $('section').hide();
}

function fragment(){
    $('this').fadeIn(1000);
}

function narration(){
    hideAllFragments();
}

$(document).ready(function(){
    narration();
    $('section').first().fadeIn(1000);
});

$('.fragment').click(function(){

    hideAllFragments();
    lancer_donut_etudiants_chercheurs();
    lancer_timeline();
    link = $(this).data('lien');
    console.log(link);
    $('[data-fragment='+link+']').fadeIn(1000);

})