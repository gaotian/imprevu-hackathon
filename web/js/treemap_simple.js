function treemap_simple(container, data) {

        var visualization = d3plus.viz()
            .container("#"+container)
            .data(data)
            .type("tree_map")
            .id("key")
            .size("value")
            .tooltip(data.key)
            .draw()
}

function treemap_clickable(container,data){

    var visualization = d3plus.viz()
        .container("#"+container)
        .data(data)
        .type("tree_map")
        .id("key")
        .size("value")
        .mouse({
            "click": function(){
                console.log();
                //select_table();
            }
        })
        .draw()



}

function select_table(containertable){

    draw_table(containertable);

}


