function vizMapParis(container, departements) {

    document.getElementById(container).innerHTML = "";

    // Taille de la carte
    var width  = 710;
    var height = 620;
    var couleurZoneHover = "red";

    var arrondissementSimple = [];

    var color = d3.scale.ordinal()
        .range(["#FFF5F0"/*2ème*/, "#67000D"/*15ème"*/, "#EF3B2C"/*18ème*/, "#FCA78A"/*16ème*/, "#FB6A4A"/*19ème*/, "#FFF5F0"/*1er*/, "#FFF0E9"/*4ème*/, "#FEE0D2"/*8ème*/, "#DD2A25"/*11ème*/, "#A50F15"/*13ème*/, "#FC9272"/*5ème*/, "#FFEBE1"/*3ème*/, "#FCBBA1"/*9ème*/, "#FFE6DA"/*7ème*/, "#CB181D"/*19ème*/, "#860811"/*20ème*/, "#B81419"/*12ème*/, "#FDCEBA"/*6ème*/, "#FC7E5E"/*10ème*/, "#F5533B"/*14ème*/]);
        //.range(["fff", "#99000D"])


    // Création du svg
    var vis = d3.select("#"+container).append("svg")
        .attr("width", width).attr("height", height);


    // Add background
    vis.append('rect')
        .attr('class', 'background')
        .attr('width', width)
        .attr('height', height)

    var infoGroupe = vis.append('g')
    //var inforect = infoGroupe.append("rect")
        .attr("x", 150)
        .attr("y", 0)
        .attr("width", 560)
        .attr("height", 40)
        .attr("fill", "red")
        .attr("id", "rectLabel");

    //var infoTexte = infoGroupe.append("text").text("Survolez un arrondissement avec la souris").attr("x" , 160).attr("y", -5).attr("dy", "2em").style("fill", "white").style("font-size", 15)

    var defs = vis.append("defs");
    var filter = defs.append("filter")
        .attr("id", "dropshadow")
    filter.append("feGaussianBlur")
        .attr("in", "SourceAlpha")
        .attr("stdDeviation",4)
        .attr("result", "blur");
    filter.append("feOffset")
        .attr("in", "blur")
        .attr("dx", 2)
        .attr("dy", 2)
        .attr("result", "offsetBlur");
    var feMerge = filter.append("feMerge");
    feMerge.append("feMergeNode")
        .attr("in", "offsetBlur")
    feMerge.append("feMergeNode")
        .attr("in", "SourceGraphic");

    var tooltips = d3.select("div#tooltips");

    function loadDataParis(arrondissements) {

            var nbPop = getPop(arrondissements);
            color.domain([d3.min(nbPop-50), d3.max(nbPop)]);

            $.each(arrondissements.sort(), function (i, item) {
                arrondissementSimple[item.zipcode] = item.Pop_par_Dept;
            });

            loadMapParis();
    };


    // Chargement du GEOjson et calcul de la projection
    function loadMapParis() {
        d3.json("/data/communes-75-paris.geojson", function(json) {

            // create a first guess for the projection
            var center = d3.geo.centroid(json)
            var scale  = 150;
            var offset = [width/2, height/2];
            var projection = d3.geo.mercator().scale(scale).center(center)
                .translate(offset);

            // create the path
            var path = d3.geo.path().projection(projection);

            // using the path determine the bounds of the current map and use
            // these to determine better values for the scale and translation
            var bounds  = path.bounds(json);
            var hscale  = scale*width  / (bounds[1][0] - bounds[0][0]);
            var vscale  = scale*height / (bounds[1][1] - bounds[0][1]);
            var scale   = (hscale < vscale) ? hscale : vscale;
            var offset  = [width - (bounds[0][0] + bounds[1][0])/2,
                height - (bounds[0][1] + bounds[1][1])/2];

            // new projection
            projection = d3.geo.mercator().center(center)
                .scale(scale).translate(offset);
            path = path.projection(projection);

            vis.selectAll("path").data(json.features).enter().append("path")
                .attr("d", path)
                .attr("id", function(d) { return 'd'+d.properties.code;}) // Ajout du code
                .on('mouseover', mouseover)
                .on('mouseout', mouseout)
                .on("mousemove", mousemove)
                //.on('click', clicked)
                .style("fill", fillFn)
                .style("stroke-width", ".1")
                .style("stroke", "black")
        });
    }

    loadDataParis(departements);



    // Get province color
    function fillFn(d){
        return color(arrondissementSimple[d.properties.code]);
    }

    function mouseover(d){
        // Highlight hovered province
        d3.select(this).style("stroke", "black").attr("filter", "url(#dropshadow)").moveToFront();
        //infoTexte.text("Code : " + d.properties.code + " - Nom : " + d.properties.nom + " - Nombre d'Emprunteurs : " + arrondissementSimple[d.properties.code]);
        tooltips.style("visibility", "visible");

    }

    function mouseout(d){
        d3.select(this).style('fill', fillFn).attr("filter", null);
    }

    function mousemove(d) {
        tooltips.style("top", (d3.event.pageY + 1) + "px").style("left", (d3.event.pageX + 16) + "px");
        tooltips.html("<h5>" + d.properties.nom + " (" + d.properties.code + ")" + "<h5> Nombre d'Emprunteurs : " + arrondissementSimple[d.properties.code] + "</h5>")
    }

    function clicked(d){
        alert("Code : " + d.properties.code + " - Nom : " + d.properties.nom + " - Emprunteurs : " + arrondissementSimple[d.properties.code]);
    }

    // Get population
    function getPop(d){
        var tab = [];
        $.each(d, function (i, item) {
            tab[i] = item.Pop_par_Dept;
        });
        return tab.sort();
    }

}